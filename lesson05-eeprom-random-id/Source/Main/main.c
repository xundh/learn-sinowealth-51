#include "SH79F9476.h"
#include "cpu.h"
#include "intrins.h"
#include "api_ext.h"
#include "clk_util.h"
#include "eeprom_random_id.h"

void main()
{
	UCHAR r[5];
	// 选择高速时钟
	highFrequenceClk();
	
	readRandomID(r);
	// 读取可读识别码
	while (1);
}
