/*##################################################################################
;# History:                                                                       #
;# V1.0        1. Original                                                        #
##################################################################################*/

#ifndef _REGISTER_H_
#define _REGISTER_H_

#include "SH79F9476.H"

#define  SETBANK0             INSCON &= 0xBF;
#define  SETBANK1             INSCON |= 0x40;

#define  RSET_WATCHDOG        RSTSTAT &= 0x78;
#define  SET_MCU_CLK_RC_24M   CLKCON = (CLKCON & 0x9F);
#define  SET_MCU_CLK_RC_12M   CLKCON = (CLKCON & 0x9F) | 0x20;

#define  Enable_Touch         TKCON1 |= 0x80;
#define  Disable_Touch        TKCON1 &= 0x7F;
#define  TK_GO                TKCON1 |= 0x40;
#define  TK_Scan_is_End      (TKF0 & 0x01) != 0x00
#define  CLR_ALL_TK_IF        TKF0 = 0x00;
#define  TK_SAMP_SEL_18CNT    TKCON1 = (TKCON1 & 0xF8) | 0x04;
#define  TK_SAMP_SEL_34CNT    TKCON1 = (TKCON1 & 0xF8) | 0x05;
#define  TK_SAMP_SEL_66CNT    TKCON1 = (TKCON1 & 0xF8) | 0x06;

#define  TK_CADJ_RAM_ADDR          0x1000
#define  TK_DATA_RAM_ADDR          0x102A
/*================================================================================================*/
#endif
