#ifndef __MCU_SYS_REGSITER_H
#define	__MCU_SYS_REGSITER_H

#include "../Inc/tk_conf_add.h"

#include "../Inc/sys_register_SH79F9476.h"  //

#if(KEY_NUMBER1 <  multikey_num)
	#error"The total number of keys is less than the number of multiple keys,please modify the total number of keys or the number of multiple keys in the configuration interface(tk_conf_add.h)"
#endif

#endif
