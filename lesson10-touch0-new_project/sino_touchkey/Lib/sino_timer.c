#include "../Lib/sino_touchkey.h"
#include "intrins.h"
U8  T30msCnt;
bit     f_5ms,f_30ms;
//=======================================================
// timer3中断程序
//=======================================================
void isr_timer3() interrupt 5
{
	_push_(INSCON);
	INSCON=0X00;  
	   
	f_5ms  = 1;
//---------------------
	if(++T30msCnt >= 6)  //6*5ms=30ms
	{
		T30msCnt = 0;
		f_30ms = 1;
	}
//---------------------
	_pop_(INSCON);
}

//=======================================================
// timer初始化
//=======================================================
void timerinit()
{
    f_5ms = 0;
    f_30ms = 0;
	T30msCnt = 0;
	SETBANK1;
	T3CON = 0x30;
    TL3 = 0x2B;  //5ms interrupt for TK
	TH3 = 0xFE;
    TR3 = 1;
    SETBANK0;
    ET3=1;       //timer3 中断使能  
}