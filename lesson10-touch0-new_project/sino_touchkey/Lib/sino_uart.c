#include "../Lib/sino_touchkey.h"
#include "../Lib/sino_uart.h"
#include "intrins.h"

U8 xdata tk_status,tk_err_num,tk_status_Tx_req;
U8 xdata chk_sum,Tx_Cnt;
U8 xdata ch_N,bl_H,bl_L,rd_H,rd_L;
U8 xdata Rx_Cnt,Rx_buf,Rx_sum,PC_Command;
U8 xdata tk_count,tk_div_H,tk_div_L;

bit  gbUartTran;
bit  PC_Ack_Ok_flag;
bit  tk_div_conf_Tx_req,tk_max_diff_conf_Tx_req,tk_data_Tx_req;
bit  fre_switch_flag_uart;

#if EN_DataAdjust
/////////////////////////////////////////////////////////////////////////////
//
// UART初始化程序，115200波特率，1位起始位，1位停止位
//
/////////////////////////////////////////////////////////////////////////////
#if (Touch_Adjust || UART_EN )
void uartinit(void)
{
	#if(UART_Sel == 2)
	  	SETBANK1
	  	SCON2 = 0x50;
	  	SBRTH2 = 0xFF;
	  	SBRTL2 = 0xF3;       //d9:38400     64:9600   F3: 115200
	  	SFINE2=0x01;
		#if Touch_Adjust 
	  	REN2 = 1;
		#else
		REN2 = 0;
		#endif
	  	RI2 = 0;
	  	SETBANK0
		//-------UART2 Port-------
		P0CR |= 0x04; 
		P0 |= 0x04;	  //TXD P0.2 OUTPUT HIGH
		#if Touch_Adjust 
		P0CR &= 0x0F7;//RXD P0.3 INPUT
		#endif	  
		//-----------------------
		#if(L_Send !=7 || Touch_Adjust)	
	  	 	IEN2|=0x02;  //enable uart 2 
		#endif 
	#endif
//---------------------------------------------------------------------------	  
	#if(UART_Sel == 1)
	 
	  	SETBANK1
	  	UART1CR = UART1_PIN;
	  	SCON1 = 0x50;
	  	SBRTH1 = 0xFF;
	  	SBRTL1 = 0xF3;       //d9:38400     64:9600   F3: 115200
	  	SFINE1=0x01;
		#if Touch_Adjust 
	  	REN1 = 1;
		#else
		REN1 = 0;
		#endif
	  	RI1 = 0;
	  	SETBANK0
	  	//-------UART1 Port-------
	  	switch(UART1_PIN&0x0F0)
	  	{
	  	    case 0x00: P0CR |= 0x40; P0 |= 0x40; break; //TXD P0.6 OUTPUT HIGH
			case 0x10: P0CR |= 0x80; P0 |= 0x80; break; //TXD P0.7 OUTPUT HIGH
			case 0x20: P1CR |= 0x10; P1 |= 0x10; break; //TXD P1.4 OUTPUT HIGH
			case 0x30: P1CR |= 0x20; P1 |= 0x20; break; //TXD P1.5 OUTPUT HIGH
			case 0x40: P2CR |= 0x40; P2 |= 0x40; break; //TXD P2.6 OUTPUT HIGH
			case 0x50: P3CR |= 0x02; P3 |= 0x02; break; //TXD P3.1 OUTPUT HIGH
			case 0x60: P3CR |= 0x04; P3 |= 0x04; break;//TXD P3.2 OUTPUT HIGH
			case 0x70: P3CR |= 0x20; P3 |= 0x20; break;//TXD P3.5 OUTPUT HIGH
			default:    break;
	  	}
		#if Touch_Adjust 
	  	switch(UART1_PIN&0x0F)
	  	{
	  		case 0x00: P0CR &= 0x0BF; break; //RXD P0.6 INPUT 
			case 0x01: P0CR &= 0x7F;  break; //RXD P0.7 INPUT 
			case 0x02: P1CR &= 0x0EF; break; //RXD P1.4 INPUT 
			case 0x03: P1CR &= 0x0DF; break; //RXD P1.5 INPUT 
			case 0x04: P2CR &= 0x0BF; break; //RXD P2.6 INPUT 
			case 0x05: P3CR &= 0x0FD;  break; //RXD P3.1 INPUT 
			case 0x06: P3CR &= 0x0FB; break; //RXD P3.2 INPUT 
			case 0x07: P3CR &= 0x0DF; break; //RXD P3.5 INPUT 
			default:    break;
	  	}
		#endif 	
	  	#if(L_Send !=7 || Touch_Adjust) 	
	  		IEN2|=0x01;  //enable uart 1 	*/
		#endif
	#endif
//---------------------------------------------------------------------------
	#if(UART_Sel ==0)
	  	SETBANK1
	  	UART0CR = UART0_PIN;
	  	SETBANK0
	  	SCON = 0x50;
	  	SBRTH = 0xFF;
	  	SBRTL = 0xF3;       //d9:38400     64:9600   F3: 115200
	  	SFINE=0x01;
		#if Touch_Adjust 
	  	REN = 1;
		#else
		REN = 0;
		#endif
	  	RI = 0;
	   	//-------UART0 Port-------
	  	switch(UART0_PIN&0x0F0)
	  	{
	  		case 0x00: P0CR |= 0x01; P0 |= 0x01; break; //TXD P0.0 OUTPUT HIGH
			case 0x10: P0CR |= 0x02; P0 |= 0x02; break; //TXD P0.1 OUTPUT HIGH
			case 0x20: P1CR |= 0x01; P1 |= 0x01; break; //TXD P1.0 OUTPUT HIGH
			case 0x30: P1CR |= 0x02; P1 |= 0x02;  break;//TXD P1.1 OUTPUT HIGH
			case 0x40: P2CR |= 0x80; P2 |= 0x80; break; //TXD P2.7 OUTPUT HIGH
			case 0x50: P3CR |= 0x01; P3 |= 0x01; break; //TXD P3.0 OUTPUT HIGH
			case 0x60: P3CR |= 0x08; P3 |= 0x08; break;//TXD P3.3 OUTPUT HIGH
			case 0x70: P3CR |= 0x10; P3 |= 0x10; break;//TXD P3.4 OUTPUT HIGH
			default:    break;
	  	}
		#if Touch_Adjust 
	    switch(UART0_PIN&0x0F)
	  	{
	  		case 0x00: P0CR &= 0x0FE; break; //RXD P0.0 INPUT 
			case 0x01: P0CR &= 0x0FD; break; //RXD P0.1 INPUT 
			case 0x02: P1CR &= 0x0FE; break; //RXD P1.0 INPUT
			case 0x03: P1CR &= 0x0FD; break; //RXD P1.1 INPUT 
			case 0x04: P2CR &= 0x7F; break; //RXD P2.2 INPUT 
			case 0x05: P3CR &= 0x0FE; break; //RXD P3.0 INPUT 
			case 0x06: P3CR &= 0x0F7; break;//RXD P3.3 INPUT
			case 0x07: P3CR &= 0x0EF; break;//RXD P3.4 INPUT
			default:    break;
	  	}
		#endif
	  	#if(L_Send !=7 || Touch_Adjust)
	  		IEN0|=0x10;  //enable uart 0  */
		#endif
	  	SETBANK0 	
	  #endif
}
#endif		  // 22行  #if (Touch_Adjust || UART_EN )
#if EN_DataAdjust
#if(Touch_Adjust || UART_EN)
#if((L_Send != 7 && L_Send != 0)||Touch_Adjust)
void start_send(void)
{
	#if (UART_Sel ==0)
		SETBANK0 
    	TI=1;
	#endif
	#if (UART_Sel ==1)
		SETBANK1
		TI1 = 1;
		SETBANK0
	#endif
	#if (UART_Sel ==2)
		SETBANK1
		TI2 = 1;
		SETBANK0
	#endif
}
#endif
#endif
#endif
#if Touch_Adjust 
///////////////////////////////////////
//
// 校准UART中断服务程序
//
///////////////////////////////////////
#if (UART_Sel ==2)
void Uart2Isr(void) interrupt 16
{
    U8 Tx_buf;
	_push_(INSCON);
	SETBANK1
    
    if(RI2)
	{
		RI2= 0;
		Rx_buf = SBUF2;
		if(Rx_buf == 0x69)
		{
			Rx_Cnt = 1;
			Rx_sum = Rx_buf;
		}
		else
		{
		    if(Rx_buf == 0x03)
		    {
		    	if(Rx_Cnt == 1)
		    	{
		    		Rx_Cnt++;
					Rx_sum += Rx_buf;
		    	}
		    	else
		    	{
		    		Rx_Cnt = 0;
		    	}
		    }
		    else
		    {
		        if(Rx_buf == 0x41)
		        {
		        	if(Rx_Cnt == 2)
		        	{
		        		Rx_Cnt++;
						Rx_sum += Rx_buf;
		        	}
		        	else
		        	{
		        		Rx_Cnt = 0;
		        	}
		        }
		        else
		        {
		            if(Rx_buf == PC_Adjust_TK)
		            {
		            	if(Rx_Cnt == 3)
		            	{
		            		Rx_Cnt++;
		            		PC_Command = PC_Adjust_TK;
							Rx_sum += Rx_buf;
		            	}
		            	else
		            	{
		            		Rx_Cnt = 0;
		            	}
		            }
					else
					{
		                if(Rx_buf == PC_Ack_Ok)
		                {
		                	if(Rx_Cnt == 3)
		                	{
		                		Rx_Cnt++;
		                		PC_Command = PC_Ack_Ok;
    							Rx_sum += Rx_buf;
		                	}
		                	else
		                	{
		                		Rx_Cnt = 0;
		                	}
		                }
					    else
					    {
		                    if(Rx_buf == PC_Req_Fre_Switch)
		                    {
		                    	if(Rx_Cnt == 3)
		                    	{
		                    		Rx_Cnt++;
		                    		PC_Command = PC_Req_Fre_Switch;
    				    			Rx_sum += Rx_buf;
		                    	}
		                    	else
		                    	{
		                    		Rx_Cnt = 0;
		                    	}
		                    }
					    	else
					    	{
		                        if(Rx_buf == Rx_sum)
		                        {
		                        	if(Rx_Cnt == 4)
		                        	{
		                        		Rx_Cnt++;
                                        if(PC_Command == PC_Adjust_TK)
		                        		{
		                        		    tk_adjust_flag = ~tk_adjust_flag;
		                        	    }
		                        	    else
		                        	    {
		                        	        if(PC_Command == PC_Ack_Ok)
		                        	        {
		                        	            PC_Ack_Ok_flag = 1;
		                        	        }
		                        	        else
		                        	        {
		                        	            fre_switch_flag_uart = 1;
		                        	        }
		                        	    }
		                        	}
		                        	else
		                        	{
		                        		Rx_Cnt = 0;
		                        	}
		                        }
					    	}						
					    }
					}
		        }
		    }
		}												
	}
	if(TI2)
	{		
		TI2 = 0;
		if(tk_status_Tx_req)
	    {
	        if(Tx_Cnt == 6)
            {
                chk_sum = 0;
                Tx_Cnt = 0;
                gbUartTran = 0;
	            tk_status_Tx_req = 0;
            }
	        
	        if(gbUartTran == 1)
	        {
	            switch(Tx_Cnt)
	            {
	                case 0:     Tx_buf = 0x69;      	chk_sum += 0x69;        break;
	                case 1:	    Tx_buf = 0x04;      	chk_sum += 0x04;        break;
	                case 2:	    Tx_buf = 0x51;      	chk_sum += 0x51;        break;
	                case 3:	    Tx_buf = tk_status;		chk_sum += tk_status;   break;
	                case 4:	    Tx_buf = tk_err_num;	chk_sum += tk_err_num;  break;
	                case 5: 	Tx_buf = chk_sum;   break;
	                default:	break;
	            }
	            
	            SBUF2 = Tx_buf;
	            Tx_Cnt++;
	        }
	    }
	    else
	    {
	        if(tk_div_conf_Tx_req)
	        {
	            if(Tx_Cnt == 7)
                {
                    chk_sum = 0;
                    Tx_Cnt = 0;
                    gbUartTran = 0;
	                tk_div_conf_Tx_req = 0;
                }
	            
	            if(gbUartTran == 1)
	            {
	                switch(Tx_Cnt)
	                {
	                    case 0:     Tx_buf = 0x69;      chk_sum += 0x69;        break;
	                    case 1:	    Tx_buf = 0x05;      chk_sum += 0x05;        break;
	                    case 2:	    Tx_buf = 0x32;      chk_sum += 0x32;        break;
	                    case 3:	    Tx_buf = tk_count;	chk_sum += tk_count;    break;
	                    case 4:	    Tx_buf = tk_div_H;  chk_sum += tk_div_H;    break;
	                    case 5:	    Tx_buf = tk_div_L;	chk_sum += tk_div_L;    break;	                        
	                    case 6: 	Tx_buf = chk_sum;   break;
	                    default:	break;
	                }
	                
	                SBUF2 = Tx_buf;
	                Tx_Cnt++;
	            }
	        }
	        else
	        {
	            if(tk_max_diff_conf_Tx_req)
	            {
	                if(Tx_Cnt == 7)
                    {
                        chk_sum = 0;
                        Tx_Cnt = 0;
                        ch_N++;
                        if(ch_N == tk_num)
                        {
                            ch_N = 0;
                            gbUartTran = 0;
							tk_max_diff_conf_Tx_req = 0;
                        }
                        else
                        {
                            bl_H = (U8)(key_max_diff[ch_N]>>8);
                            bl_L = (U8)(key_max_diff[ch_N]);
                        }
                    }
	                
	                if(gbUartTran == 1)
	                {
	                    switch(Tx_Cnt)
	                    {
	                        case 0:     Tx_buf = 0x69;      chk_sum += 0x69;      break;
	                        case 1:	    Tx_buf = 0x05;      chk_sum += 0x05;      break;
	                        case 2:	    Tx_buf = 0x22;      chk_sum += 0x22;      break;
	                        case 3:	    Tx_buf = ch_N;   	chk_sum += ch_N;      break;
	                        case 4:	    Tx_buf = bl_H;      chk_sum += bl_H;      break;
	                        case 5:	    Tx_buf = bl_L;      chk_sum += bl_L;      break;
	                        case 6:	    Tx_buf = chk_sum;   break;
	                        default:	break;
	                    }
	                    
	                    SBUF2 = Tx_buf;
	                    Tx_Cnt++;
	                }
	            }
	            else
	            {
	                if(tk_data_Tx_req)
					{
					    if(Tx_Cnt == 11)
                        {
                            chk_sum = 0;
                            Tx_Cnt = 0;
                            ch_N++;
                            if(ch_N == tk_num)
                            {
                                ch_N = 0;
                                gbUartTran = 0;
                            }
                            else
                            {
                                bl_H = (U8)(LF_key_baseline[ch_N]>>8);	
                                bl_L = (U8)(LF_key_baseline[ch_N]);		  
                                rd_H = (U8)(LF_key[ch_N]>>8);				
                                rd_L = (U8)(LF_key[ch_N]);					
                            }
                        }
	                    
	                    if(gbUartTran == 1)
	                    {
	                        switch(Tx_Cnt)
	                        {
	                            case 0:     Tx_buf = 0x69;      chk_sum += 0x69;      break;
	                            case 1:	    Tx_buf = 0x09;      chk_sum += 0x09;      break;
	                            case 2:	    Tx_buf = 0x02;      chk_sum += 0x02;      break;
	                            case 3:	    Tx_buf = ch_N;   	chk_sum += ch_N;      break;
	                            case 4:	    Tx_buf = bl_H;      chk_sum += bl_H;      break;
	                            case 5:	    Tx_buf = bl_L;      chk_sum += bl_L;      break;
	                            case 6:	    Tx_buf = 0x12;      chk_sum += 0x12;      break;
	                            case 7:	    Tx_buf = ch_N;      chk_sum += ch_N;      break;
	                            case 8:	    Tx_buf = rd_H;      chk_sum += rd_H;      break;
	                            case 9:	    Tx_buf = rd_L;      chk_sum += rd_L;      break;
	                            case 10:	Tx_buf = chk_sum;   break;
	                            default:	break;
	                        }
	                        
	                        SBUF2 = Tx_buf;
	                        Tx_Cnt++;
	                    }
	                }
					else
					{
						gbUartTran = 0;
					}
	            }
	        }
	    }
	}
	SETBANK0 
	_pop_(INSCON);
}

#endif
#if (UART_Sel ==1)
void Uart1Isr(void) interrupt 15
{
    U8 Tx_buf;
	_push_(INSCON);
	SETBANK1
    
    if(RI1)
	{
		RI1= 0;
		Rx_buf = SBUF1;
		if(Rx_buf == 0x69)
		{
			Rx_Cnt = 1;
			Rx_sum = Rx_buf;
		}
		else
		{
		    if(Rx_buf == 0x03)
		    {
		    	if(Rx_Cnt == 1)
		    	{
		    		Rx_Cnt++;
					Rx_sum += Rx_buf;
		    	}
		    	else
		    	{
		    		Rx_Cnt = 0;
		    	}
		    }
		    else
		    {
		        if(Rx_buf == 0x41)
		        {
		        	if(Rx_Cnt == 2)
		        	{
		        		Rx_Cnt++;
						Rx_sum += Rx_buf;
		        	}
		        	else
		        	{
		        		Rx_Cnt = 0;
		        	}
		        }
		        else
		        {
		            if(Rx_buf == PC_Adjust_TK)
		            {
		            	if(Rx_Cnt == 3)
		            	{
		            		Rx_Cnt++;
		            		PC_Command = PC_Adjust_TK;
							Rx_sum += Rx_buf;
		            	}
		            	else
		            	{
		            		Rx_Cnt = 0;
		            	}
		            }
					else
					{
		                if(Rx_buf == PC_Ack_Ok)
		                {
		                	if(Rx_Cnt == 3)
		                	{
		                		Rx_Cnt++;
		                		PC_Command = PC_Ack_Ok;
    							Rx_sum += Rx_buf;
		                	}
		                	else
		                	{
		                		Rx_Cnt = 0;
		                	}
		                }
					    else
					    {
		                    if(Rx_buf == PC_Req_Fre_Switch)
		                    {
		                    	if(Rx_Cnt == 3)
		                    	{
		                    		Rx_Cnt++;
		                    		PC_Command = PC_Req_Fre_Switch;
    				    			Rx_sum += Rx_buf;
		                    	}
		                    	else
		                    	{
		                    		Rx_Cnt = 0;
		                    	}
		                    }
					    	else
					    	{
		                        if(Rx_buf == Rx_sum)
		                        {
		                        	if(Rx_Cnt == 4)
		                        	{
		                        		Rx_Cnt++;
                                        if(PC_Command == PC_Adjust_TK)
		                        		{
		                        		    tk_adjust_flag = ~tk_adjust_flag;
		                        	    }
		                        	    else
		                        	    {
		                        	        if(PC_Command == PC_Ack_Ok)
		                        	        {
		                        	            PC_Ack_Ok_flag = 1;
		                        	        }
		                        	        else
		                        	        {
		                        	            fre_switch_flag_uart = 1;
		                        	        }
		                        	    }
		                        	}
		                        	else
		                        	{
		                        		Rx_Cnt = 0;
		                        	}
		                        }
					    	}						
					    }
					}
		        }
		    }
		}												
	}
	if(TI1)
	{		
		TI1 = 0;
		if(tk_status_Tx_req)
	    {
	        if(Tx_Cnt == 6)
            {
                chk_sum = 0;
                Tx_Cnt = 0;
                gbUartTran = 0;
	            tk_status_Tx_req = 0;
            }
	        
	        if(gbUartTran == 1)
	        {
	            switch(Tx_Cnt)
	            {
	                case 0:     Tx_buf = 0x69;      	chk_sum += 0x69;        break;
	                case 1:	    Tx_buf = 0x04;      	chk_sum += 0x04;        break;
	                case 2:	    Tx_buf = 0x51;      	chk_sum += 0x51;        break;
	                case 3:	    Tx_buf = tk_status;		chk_sum += tk_status;   break;
	                case 4:	    Tx_buf = tk_err_num;	chk_sum += tk_err_num;  break;
	                case 5: 	Tx_buf = chk_sum;   break;
	                default:	break;
	            }
	            
	            SBUF1 = Tx_buf;
	            Tx_Cnt++;
	        }
	    }
	    else
	    {
	        if(tk_div_conf_Tx_req)
	        {
	            if(Tx_Cnt == 7)
                {
                    chk_sum = 0;
                    Tx_Cnt = 0;
                    gbUartTran = 0;
	                tk_div_conf_Tx_req = 0;
                }
	            
	            if(gbUartTran == 1)
	            {
	                switch(Tx_Cnt)
	                {
	                    case 0:     Tx_buf = 0x69;      chk_sum += 0x69;        break;
	                    case 1:	    Tx_buf = 0x05;      chk_sum += 0x05;        break;
	                    case 2:	    Tx_buf = 0x32;      chk_sum += 0x32;        break;
	                    case 3:	    Tx_buf = tk_count;	chk_sum += tk_count;    break;
	                    case 4:	    Tx_buf = tk_div_H;  chk_sum += tk_div_H;    break;
	                    case 5:	    Tx_buf = tk_div_L;	chk_sum += tk_div_L;    break;	                        
	                    case 6: 	Tx_buf = chk_sum;   break;
	                    default:	break;
	                }
	                
	                SBUF1 = Tx_buf;
	                Tx_Cnt++;
	            }
	        }
	        else
	        {
	            if(tk_max_diff_conf_Tx_req)
	            {
	                if(Tx_Cnt == 7)
                    {
                        chk_sum = 0;
                        Tx_Cnt = 0;
                        ch_N++;
                        if(ch_N == tk_num)
                        {
                            ch_N = 0;
                            gbUartTran = 0;
							tk_max_diff_conf_Tx_req = 0;
                        }
                        else
                        {
                            bl_H = (U8)(key_max_diff[ch_N]>>8);
                            bl_L = (U8)(key_max_diff[ch_N]);
                        }
                    }
	                
	                if(gbUartTran == 1)
	                {
	                    switch(Tx_Cnt)
	                    {
	                        case 0:     Tx_buf = 0x69;      chk_sum += 0x69;      break;
	                        case 1:	    Tx_buf = 0x05;      chk_sum += 0x05;      break;
	                        case 2:	    Tx_buf = 0x22;      chk_sum += 0x22;      break;
	                        case 3:	    Tx_buf = ch_N;   	chk_sum += ch_N;      break;
	                        case 4:	    Tx_buf = bl_H;      chk_sum += bl_H;      break;
	                        case 5:	    Tx_buf = bl_L;      chk_sum += bl_L;      break;
	                        case 6:	    Tx_buf = chk_sum;   break;
	                        default:	break;
	                    }
	                    
	                    SBUF1 = Tx_buf;
	                    Tx_Cnt++;
	                }
	            }
	            else
	            {
	                if(tk_data_Tx_req)
					{
					    if(Tx_Cnt == 11)
                        {
                            chk_sum = 0;
                            Tx_Cnt = 0;
                            ch_N++;
                            if(ch_N == tk_num)
                            {
                                ch_N = 0;
                                gbUartTran = 0;
                            }
                            else
                            {
                                bl_H = (U8)(LF_key_baseline[ch_N]>>8);	  
                                bl_L = (U8)(LF_key_baseline[ch_N]);		 
                                rd_H = (U8)(LF_key[ch_N]>>8);				
                                rd_L = (U8)(LF_key[ch_N]);					
                            }
                        }
	                    
	                    if(gbUartTran == 1)
	                    {
	                        switch(Tx_Cnt)
	                        {
	                            case 0:     Tx_buf = 0x69;      chk_sum += 0x69;      break;
	                            case 1:	    Tx_buf = 0x09;      chk_sum += 0x09;      break;
	                            case 2:	    Tx_buf = 0x02;      chk_sum += 0x02;      break;
	                            case 3:	    Tx_buf = ch_N;   	chk_sum += ch_N;      break;
	                            case 4:	    Tx_buf = bl_H;      chk_sum += bl_H;      break;
	                            case 5:	    Tx_buf = bl_L;      chk_sum += bl_L;      break;
	                            case 6:	    Tx_buf = 0x12;      chk_sum += 0x12;      break;
	                            case 7:	    Tx_buf = ch_N;      chk_sum += ch_N;      break;
	                            case 8:	    Tx_buf = rd_H;      chk_sum += rd_H;      break;
	                            case 9:	    Tx_buf = rd_L;      chk_sum += rd_L;      break;
	                            case 10:	Tx_buf = chk_sum;   break;
	                            default:	break;
	                        }
	                        
	                        SBUF1 = Tx_buf;
	                        Tx_Cnt++;
	                    }
	                }
					else
					{
						gbUartTran = 0;
					}
	            }
	        }
	    }
	}
	SETBANK0 
	_pop_(INSCON);
}

#endif
#if (UART_Sel ==0)
void Uart0Isr(void) interrupt 4
{
    U8 Tx_buf;
	_push_(INSCON);
	SETBANK0
    
    if(RI)
	{
		RI= 0;
		Rx_buf = SBUF;
		if(Rx_buf == 0x69)
		{
			Rx_Cnt = 1;
			Rx_sum = Rx_buf;
		}
		else
		{
		    if(Rx_buf == 0x03)
		    {
		    	if(Rx_Cnt == 1)
		    	{
		    		Rx_Cnt++;
					Rx_sum += Rx_buf;
		    	}
		    	else
		    	{
		    		Rx_Cnt = 0;
		    	}
		    }
		    else
		    {
		        if(Rx_buf == 0x41)
		        {
		        	if(Rx_Cnt == 2)
		        	{
		        		Rx_Cnt++;
						Rx_sum += Rx_buf;
		        	}
		        	else
		        	{
		        		Rx_Cnt = 0;
		        	}
		        }
		        else
		        {
		            if(Rx_buf == PC_Adjust_TK)
		            {
		            	if(Rx_Cnt == 3)
		            	{
		            		Rx_Cnt++;
		            		PC_Command = PC_Adjust_TK;
							Rx_sum += Rx_buf;
		            	}
		            	else
		            	{
		            		Rx_Cnt = 0;
		            	}
		            }
					else
					{
		                if(Rx_buf == PC_Ack_Ok)
		                {
		                	if(Rx_Cnt == 3)
		                	{
		                		Rx_Cnt++;
		                		PC_Command = PC_Ack_Ok;
    							Rx_sum += Rx_buf;
		                	}
		                	else
		                	{
		                		Rx_Cnt = 0;
		                	}
		                }
					    else
					    {
		                    if(Rx_buf == PC_Req_Fre_Switch)
		                    {
		                    	if(Rx_Cnt == 3)
		                    	{
		                    		Rx_Cnt++;
		                    		PC_Command = PC_Req_Fre_Switch;
    				    			Rx_sum += Rx_buf;
		                    	}
		                    	else
		                    	{
		                    		Rx_Cnt = 0;
		                    	}
		                    }
					    	else
					    	{
		                        if(Rx_buf == Rx_sum)
		                        {
		                        	if(Rx_Cnt == 4)
		                        	{
		                        		Rx_Cnt++;
                                        if(PC_Command == PC_Adjust_TK)
		                        		{
		                        		    tk_adjust_flag = ~tk_adjust_flag;
		                        	    }
		                        	    else
		                        	    {
		                        	        if(PC_Command == PC_Ack_Ok)
		                        	        {
		                        	            PC_Ack_Ok_flag = 1;
		                        	        }
		                        	        else
		                        	        {
		                        	            fre_switch_flag_uart = 1;
		                        	        }
		                        	    }
		                        	}
		                        	else
		                        	{
		                        		Rx_Cnt = 0;
		                        	}
		                        }
					    	}						
					    }
					}
		        }
		    }
		}												
	}
	if(TI)
	{		
		TI = 0;
		if(tk_status_Tx_req)
	    {
	        if(Tx_Cnt == 6)
            {
                chk_sum = 0;
                Tx_Cnt = 0;
                gbUartTran = 0;
	            tk_status_Tx_req = 0;
            }
	        
	        if(gbUartTran == 1)
	        {
	            switch(Tx_Cnt)
	            {
	                case 0:     Tx_buf = 0x69;      	chk_sum += 0x69;        break;
	                case 1:	    Tx_buf = 0x04;      	chk_sum += 0x04;        break;
	                case 2:	    Tx_buf = 0x51;      	chk_sum += 0x51;        break;
	                case 3:	    Tx_buf = tk_status;		chk_sum += tk_status;   break;
	                case 4:	    Tx_buf = tk_err_num;	chk_sum += tk_err_num;  break;
	                case 5: 	Tx_buf = chk_sum;   break;
	                default:	break;
	            }
	            
	            SBUF = Tx_buf;
	            Tx_Cnt++;
	        }
	    }
	    else
	    {
	        if(tk_div_conf_Tx_req)
	        {
	            if(Tx_Cnt == 7)
                {
                    chk_sum = 0;
                    Tx_Cnt = 0;
                    gbUartTran = 0;
	                tk_div_conf_Tx_req = 0;
                }
	            
	            if(gbUartTran == 1)
	            {
	                switch(Tx_Cnt)
	                {
	                    case 0:     Tx_buf = 0x69;      chk_sum += 0x69;        break;
	                    case 1:	    Tx_buf = 0x05;      chk_sum += 0x05;        break;
	                    case 2:	    Tx_buf = 0x32;      chk_sum += 0x32;        break;
	                    case 3:	    Tx_buf = tk_count;	chk_sum += tk_count;    break;
	                    case 4:	    Tx_buf = tk_div_H;  chk_sum += tk_div_H;    break;
	                    case 5:	    Tx_buf = tk_div_L;	chk_sum += tk_div_L;    break;	                        
	                    case 6: 	Tx_buf = chk_sum;   break;
	                    default:	break;
	                }
	                
	                SBUF = Tx_buf;
	                Tx_Cnt++;
	            }
	        }
	        else
	        {
	            if(tk_max_diff_conf_Tx_req)
	            {
	                if(Tx_Cnt == 7)
                    {
                        chk_sum = 0;
                        Tx_Cnt = 0;
                        ch_N++;
                        if(ch_N == tk_num)
                        {
                            ch_N = 0;
                            gbUartTran = 0;
							tk_max_diff_conf_Tx_req = 0;
                        }
                        else
                        {
                            bl_H = (U8)(key_max_diff[ch_N]>>8);
                            bl_L = (U8)(key_max_diff[ch_N]);
                        }
                    }
	                
	                if(gbUartTran == 1)
	                {
	                    switch(Tx_Cnt)
	                    {
	                        case 0:     Tx_buf = 0x69;      chk_sum += 0x69;      break;
	                        case 1:	    Tx_buf = 0x05;      chk_sum += 0x05;      break;
	                        case 2:	    Tx_buf = 0x22;      chk_sum += 0x22;      break;
	                        case 3:	    Tx_buf = ch_N;   	chk_sum += ch_N;      break;
	                        case 4:	    Tx_buf = bl_H;      chk_sum += bl_H;      break;
	                        case 5:	    Tx_buf = bl_L;      chk_sum += bl_L;      break;
	                        case 6:	    Tx_buf = chk_sum;   break;
	                        default:	break;
	                    }
	                    
	                    SBUF = Tx_buf;
	                    Tx_Cnt++;
	                }
	            }
	            else
	            {
	                if(tk_data_Tx_req)
					{
					    if(Tx_Cnt == 11)
                        {
                            chk_sum = 0;
                            Tx_Cnt = 0;
                            ch_N++;
                            if(ch_N == tk_num)
                            {
                                ch_N = 0;
                                gbUartTran = 0;
                            }
                            else
                            {
                                bl_H = (U8)(LF_key_baseline[ch_N]>>8);	  
                                bl_L = (U8)(LF_key_baseline[ch_N]);		  
                                rd_H = (U8)(LF_key[ch_N]>>8);				
                                rd_L = (U8)(LF_key[ch_N]);				
                            }
                        }
	                    
	                    if(gbUartTran == 1)
	                    {
	                        switch(Tx_Cnt)
	                        {
	                            case 0:     Tx_buf = 0x69;      chk_sum += 0x69;      break;
	                            case 1:	    Tx_buf = 0x09;      chk_sum += 0x09;      break;
	                            case 2:	    Tx_buf = 0x02;      chk_sum += 0x02;      break;
	                            case 3:	    Tx_buf = ch_N;   	chk_sum += ch_N;      break;
	                            case 4:	    Tx_buf = bl_H;      chk_sum += bl_H;      break;
	                            case 5:	    Tx_buf = bl_L;      chk_sum += bl_L;      break;
	                            case 6:	    Tx_buf = 0x12;      chk_sum += 0x12;      break;
	                            case 7:	    Tx_buf = ch_N;      chk_sum += ch_N;      break;
	                            case 8:	    Tx_buf = rd_H;      chk_sum += rd_H;      break;
	                            case 9:	    Tx_buf = rd_L;      chk_sum += rd_L;      break;
	                            case 10:	Tx_buf = chk_sum;   break;
	                            default:	break;
	                        }
	                        
	                        SBUF = Tx_buf;
	                        Tx_Cnt++;
	                    }
	                }
					else
					{
						gbUartTran = 0;
					}
	            }
	        }
	    }
	}
	SETBANK0 
	_pop_(INSCON);
}

#endif
///////////////////////////////////////
//
// 校准UART发送触摸按键基值和键值
//
///////////////////////////////////////
void UartTxd(void)
{
	if(gbUartTran == 0)
	{
        chk_sum = 0;
        Tx_Cnt =0;
        ch_N = 0;
        bl_H = (U8)(LF_key_baseline[ch_N]>>8); 
        bl_L = (U8)(LF_key_baseline[ch_N]);	  
        rd_H = (U8)(LF_key[ch_N]>>8);
        rd_L = (U8)(LF_key[ch_N]);
	    gbUartTran = 1;
		
		start_send();

    }
}

///////////////////////////////////////
//
// 校准UART发送触摸按键异常状态
//
///////////////////////////////////////
void UartTxd_Status(U8 tk_sts)
{
	PC_Ack_Ok_flag = 0;
	while(gbUartTran);
	if(tk_status_Tx_req == 0)
	{
		tk_status_Tx_req = 1;
		tk_status = tk_sts;
		tk_err_num = 0;
//		tk_err_num = TKW;
		UartTxd();
	}
}
 
///////////////////////////////////////////////////////////////////////////////////////////////
//==========================================================================
//	   串口调试相关函数
//==========================================================================
#else		 //line 168  #if Touch_Adjust 

#if UART_EN
//------------------------------------------------------
#if ((L_Send>0) && (L_Send<7))
///////////////////////////////////////
//
// UART中断服务程序
//
///////////////////////////////////////
#if (UART_Sel ==0)
void Uart0Isr(void) interrupt 4
{
    U8  Tx_buf;
	_push_(INSCON);
	SETBANK0
	if(TI)
	{		
		TI = 0;
	    if(1)	  
		{
			if(Tx_Cnt == 11)
            {
				chk_sum = 0;
                Tx_Cnt = 0;
                ch_N++;
			 #if (L_Send<2)
                 if(ch_N == tk_num)
                 {
					ch_N = 0;
                    gbUartTran = 0;
                 }
			 #endif
			 #if (L_Send >1) 
	             if(ch_N == ((tk_num<<1)+1))
	             {
					 ch_N = 0;
	                 gbUartTran = 0;
	             }			
			 #endif
                else
                {
					#if(L_Send ==1)	//发送低频 基线值+滤波后的值
			 			bl_H = (U8)(LF_key_baseline[ch_N]>>8);
                   		bl_L = (U8)(LF_key_baseline[ch_N]);
                    	rd_H = (U8)(LF_key[ch_N]>>8);
                    	rd_L = (U8)(LF_key[ch_N]);
					#endif
					#if(L_Send ==2)	//发送低频基线值+限幅后的值
					   if(ch_N <tk_num)
					   {
				          bl_H = (U8)(LF_key_baseline[ch_N]>>8);
        				  bl_L = (U8)(LF_key_baseline[ch_N]);
        				  rd_H = (U8)(LF_key[ch_N]>>8);
        				  rd_L = (U8)(LF_key[ch_N]);
					   }
					   else	if(ch_N ==  (tk_num<<1))
					   {
				   		  bl_H = (U8)(sino_key_value>>8);
        				  bl_L = (U8)(sino_key_value);
        				  rd_H = (U8)(sino_key_value>>8);
        				  rd_L = (U8)(sino_key_value);
					   }
					   else
					   {
				          bl_H = (U8)(HF_key_baseline[ch_N-tk_num]>>8);
        				  bl_L = (U8)(HF_key_baseline[ch_N-tk_num]);
        				  rd_H = (U8)(HF_key[ch_N-tk_num]>>8);
        				  rd_L = (U8)(HF_key[ch_N-tk_num]);						   		
					   }
					#endif
                }
			}
	        if(gbUartTran == 1)
	        {
				switch(Tx_Cnt)
	            {
					case 0:     Tx_buf = 0x69;      chk_sum += 0x69;      break;
	                case 1:	    Tx_buf = 0x09;      chk_sum += 0x09;      break;
	                case 2:	    Tx_buf = 0x02;      chk_sum += 0x02;      break;
	                case 3:	    Tx_buf = ch_N;   	chk_sum += ch_N;      break;
	                case 4:	    Tx_buf = bl_H;      chk_sum += bl_H;      break;
	                case 5:	    Tx_buf = bl_L;      chk_sum += bl_L;      break;
	                case 6:	    Tx_buf = 0x12;      chk_sum += 0x12;      break;
	                case 7:	    Tx_buf = ch_N;      chk_sum += ch_N;      break;
	                case 8:	    Tx_buf = rd_H;      chk_sum += rd_H;      break;
	                case 9:	    Tx_buf = rd_L;      chk_sum += rd_L;      break;
	                case 10:	Tx_buf = chk_sum;   break;
	                default:	break;
	             }
	             SBUF = Tx_buf;
	             Tx_Cnt++;
	         }

	    }	        	    
	}
	SETBANK0
	_pop_(INSCON);
}
#endif
#if (UART_Sel ==1)
void Uart1Isr(void) interrupt 15
{
    U8  Tx_buf;
	_push_(INSCON);
	SETBANK1
	if(TI1)
	{		
		TI1 = 0;
	    if(1)	       
		{
			if(Tx_Cnt == 11)
            {
				chk_sum = 0;
                Tx_Cnt = 0;
                ch_N++;
			 #if (L_Send<2)
                 if(ch_N == tk_num)
                 {
					ch_N = 0;
                    gbUartTran = 0;
                 }
			 #endif
			 #if (L_Send >1) 
	             if(ch_N == tk_num*2)
	             {
					  ch_N = 0;
	                 gbUartTran = 0;
	             }			
			 #endif
                else
                {
					#if(L_Send ==1)	//发送低频 基线值+滤波后的值
			 			bl_H = (U8)(LF_key_baseline[ch_N]>>8);
                   		bl_L = (U8)(LF_key_baseline[ch_N]);
                    	rd_H = (U8)(LF_key[ch_N]>>8);
                    	rd_L = (U8)(LF_key[ch_N]);	
					#endif
					#if(L_Send ==2)	//发送低频原始值+滤波后的值
					   if(ch_N <tk_num)
					   {
				          bl_H = (U8)(LF_key_baseline[ch_N]>>8);
        				  bl_L = (U8)(LF_key_baseline[ch_N]);
        				  rd_H = (U8)(LF_key[ch_N]>>8);
        				  rd_L = (U8)(LF_key[ch_N]);
					   }
					   else
					   {
				          bl_H = (U8)(HF_key_baseline[ch_N-tk_num]>>8);
        				  bl_L = (U8)(HF_key_baseline[ch_N-tk_num]);
        				  rd_H = (U8)(HF_key[ch_N-tk_num]>>8);
        				  rd_L = (U8)(HF_key[ch_N-tk_num]);					   	
					   }
					#endif
                }
			}
	        if(gbUartTran == 1)
	        {
				switch(Tx_Cnt)
	            {
					case 0:     Tx_buf = 0x69;      chk_sum += 0x69;      break;
	                case 1:	    Tx_buf = 0x09;      chk_sum += 0x09;      break;
	                case 2:	    Tx_buf = 0x02;      chk_sum += 0x02;      break;
	                case 3:	    Tx_buf = ch_N;   	chk_sum += ch_N;      break;
	                case 4:	    Tx_buf = bl_H;      chk_sum += bl_H;      break;
	                case 5:	    Tx_buf = bl_L;      chk_sum += bl_L;      break;
	                case 6:	    Tx_buf = 0x12;      chk_sum += 0x12;      break;
	                case 7:	    Tx_buf = ch_N;      chk_sum += ch_N;      break;
	                case 8:	    Tx_buf = rd_H;      chk_sum += rd_H;      break;
	                case 9:	    Tx_buf = rd_L;      chk_sum += rd_L;      break;
	                case 10:	Tx_buf = chk_sum;   break;
	                default:	break;
	             }
	             SBUF1 = Tx_buf;
	             Tx_Cnt++;
	          }

	    }	        	    
	}
	SETBANK0
	_pop_(INSCON);
}
#endif
#if (UART_Sel ==2)
void Uart2Isr(void) interrupt 16
{
 U8  Tx_buf;
	_push_(INSCON);
	SETBANK1
	if(TI2)
	{		
		TI2 = 0;
	    if(1)	       
		{
			if(Tx_Cnt == 11)
            {
				chk_sum = 0;
                Tx_Cnt = 0;
                ch_N++;
			 #if (L_Send<2)
                 if(ch_N == tk_num)
                 {
					ch_N = 0;
                    gbUartTran = 0;
                 }
			 #endif
			 #if (L_Send >1) 
	             if(ch_N == tk_num*2)
	             {
					  ch_N = 0;
	                 gbUartTran = 0;
	             }			
			 #endif
                else
                {
					#if(L_Send ==1)	//发送低频 基线值+滤波后的值
			 			bl_H = (U8)(LF_key_baseline[ch_N]>>8);
                   		bl_L = (U8)(LF_key_baseline[ch_N]);
                    	rd_H = (U8)(LF_key[ch_N]>>8);
                    	rd_L = (U8)(LF_key[ch_N]);	
					#endif
					#if(L_Send ==2)	//发送低频原始值+滤波后的值
					   if(ch_N <tk_num)
					   {
				          bl_H = (U8)(LF_key_baseline[ch_N]>>8);
        				  bl_L = (U8)(LF_key_baseline[ch_N]);
        				  rd_H = (U8)(LF_key[ch_N]>>8);
        				  rd_L = (U8)(LF_key[ch_N]);
					   }
					   else
					   {
				          bl_H = (U8)(HF_key_baseline[ch_N-tk_num]>>8);
        				  bl_L = (U8)(HF_key_baseline[ch_N-tk_num]);
        				  rd_H = (U8)(HF_key[ch_N-tk_num]>>8);
        				  rd_L = (U8)(HF_key[ch_N-tk_num]);					   	
					   }
					#endif
                }
			}
	        if(gbUartTran == 1)
	        {
				switch(Tx_Cnt)
	            {
					case 0:     Tx_buf = 0x69;      chk_sum += 0x69;      break;
	                case 1:	    Tx_buf = 0x09;      chk_sum += 0x09;      break;
	                case 2:	    Tx_buf = 0x02;      chk_sum += 0x02;      break;
	                case 3:	    Tx_buf = ch_N;   	chk_sum += ch_N;      break;
	                case 4:	    Tx_buf = bl_H;      chk_sum += bl_H;      break;
	                case 5:	    Tx_buf = bl_L;      chk_sum += bl_L;      break;
	                case 6:	    Tx_buf = 0x12;      chk_sum += 0x12;      break;
	                case 7:	    Tx_buf = ch_N;      chk_sum += ch_N;      break;
	                case 8:	    Tx_buf = rd_H;      chk_sum += rd_H;      break;
	                case 9:	    Tx_buf = rd_L;      chk_sum += rd_L;      break;
	                case 10:	Tx_buf = chk_sum;   break;
	                default:	break;
	             }
	             SBUF2 = Tx_buf;
	             Tx_Cnt++;
	          }

	    }	        	    
	}
	SETBANK0
	_pop_(INSCON);
}
#endif
/////////////////////////////////////////////////////////////////
//
// UART发送触摸按键基值和键值
//
////////////////////////////////////////////////////////////////
void UartTxd(void)	 
{
	if(gbUartTran == 0)
	{
        chk_sum = 0;
        Tx_Cnt =0;
        ch_N = 0;

		#if(L_Send ==1)	  //发送低频 基线值+滤波后的值
			bl_H = (U8)(LF_key_baseline[ch_N]>>8);
            bl_L = (U8)(LF_key_baseline[ch_N]);
            rd_H = (U8)(LF_key[ch_N]>>8);
            rd_L = (U8)(LF_key[ch_N]);
		#endif
		#if (L_Send ==2)  //发送低频原始值+滤波后的值
        	bl_H = (U8)(LF_key_baseline[ch_N]>>8);
        	bl_L = (U8)(LF_key_baseline[ch_N]);
        	rd_H = (U8)(LF_key[ch_N]>>8);
        	rd_L = (U8)(LF_key[ch_N]);
		#endif

	    gbUartTran = 1;
	 //-----------------------------------------
	 	start_send(); 
	 //-----------------------------------------		
    }
}
#endif		 //line 1070 #if ((L_Send>0) && (L_Send<7))
//------------------------------------------------------	
#endif		 //"line 168  #if Touch_Adjust" else "line 1068 #if UART_EN "

#endif 		 //	line 168  #if Touch_Adjust

#endif		// line 16 #if EN_DataAdjust
