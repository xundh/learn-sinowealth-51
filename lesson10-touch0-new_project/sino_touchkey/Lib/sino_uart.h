#ifndef __UART_H
#define	__UART_H

#define	PC_Adjust_TK		0x01
#define	PC_Ack_Ok			0x02
#define	PC_Req_Fre_Switch	0x07

extern U8 xdata chk_sum,Tx_Cnt;
extern U8 xdata ch_N,bl_H,bl_L,rd_H,rd_L,tk_F;
extern U8 xdata Rx_Cnt,Rx_buf;
extern U8 xdata tk_count,tk_div_H,tk_div_L;

extern bit  gbUartTran;
extern bit 	PC_Ack_Ok_flag;
extern bit 	tk_div_conf_Tx_req,tk_max_diff_conf_Tx_req,tk_data_Tx_req;

extern void uartinit();
extern void UartTxd();
extern void UartTxd_Status(U8 tk_sts);
extern void UartTxd_new_tk_conf();
extern void start_send();

#endif