#include "../Lib/sino_touchkey.h"
#include "intrins.h"
/*==============new frequency jump========================*/
U8  xdata  Fre_num_L=0;
U16 xdata  key_filter_L[KEY_NUMBER1][Filter_Frequency];
U16 xdata  tempdata_L[Filter_Frequency];
U8  xdata  Frequcecy_div_set;
U8  xdata  Frequcecy_filter_set;
U8 	xdata  key_fastcounter;
U8  xdata  Fre_switch_Cnt;
U8  xdata  Fre_switch_Set;

U8  xdata  R_TKstep_Fre_Lmsg;
U8  xdata  R_TKstep_Fre_Hmsg;
U8  xdata  R_TKstep_Fre_H_step3_6;
U8  xdata  R_Fre_ST;
//=========================================================
#if ((Function&0x01) ==0x01)
xdata U8 R_water_add_count;
xdata U8 R_water_Delay ;
xdata U8 R_water_num ;
xdata U8 R_Inter1 ;
#endif
/*==================新增功能==============================*/
U8  xdata  Add_Set = 0;
U8  xdata  Add_function;
/*========================================================*/
U8  xdata  LF_key_WtCnt;
U8  xdata  key_WT_set;
U8  xdata  FLAG_COUNT_set;
U8  xdata  init_BaseCnt_set;
U8  xdata  key_sample_count;
U8  xdata  tk_num;
U16 xdata  LF_DIV[KEY_NUMBER1];
U16 xdata  HF_DIV[KEY_NUMBER1];
U32 xdata  sino_key_value;
U32 xdata  sino_key_value_pre;
U8  xdata  multikey_control;
U8  xdata  line;  
U8  xdata  R_downCnt;
U8  xdata  datasub_counter;
U8  xdata  dataplus_counter;
U16 xdata  R_Reset_Cnt;
U16 xdata  R_Reset_Cnt_H;
U16 xdata  R_HF_MaxLimit;
U8  xdata  R_data_add;
U8  xdata  R_Cnt_WT;
/*======================================================*/
U16 xdata HF_key_baseline[KEY_NUMBER1];
U16 xdata HF_key[KEY_NUMBER1];
#if Touch_Adjust==1		
U16 xdata HF_key_max_diff[KEY_NUMBER1];
#endif
U16 xdata HF_key_filter[KEY_NUMBER1][Filter_Deg];
U8 	xdata HF_keytouch_flag[KEY_NUMBER1];
U8 	xdata HF_key_flag[KEY_NUMBER1];
U8 	xdata HF_baseline_sub_flag[KEY_NUMBER1];
U8 	xdata HF_baseline_plus_flag[KEY_NUMBER1];
U8 	xdata HF_key_count_add[KEY_NUMBER1];
U8 	xdata HF_key_count_sub[KEY_NUMBER1];

U16 xdata LF_key_baseline[KEY_NUMBER1];
U16 xdata LF_key[KEY_NUMBER1];
#if Touch_Adjust==1
U16 xdata LF_key_max_diff[KEY_NUMBER1];
#endif
U16 xdata LF_key_filter[KEY_NUMBER1][Filter_Deg];
U8 	xdata LF_keytouch_flag[KEY_NUMBER1];
U8 	xdata LF_key_flag[KEY_NUMBER1];
U8 	xdata LF_baseline_sub_flag[KEY_NUMBER1];
U8 	xdata LF_baseline_plus_flag[KEY_NUMBER1];
U8 	xdata LF_key_count_add[KEY_NUMBER1];
U8 	xdata LF_key_count_sub[KEY_NUMBER1];
U8  xdata datasub_counter;
U8  xdata dataplus_counter;
U8  xdata line;  
U16 xdata LF_key_threshold2;
U16 xdata LF_key_threshold3;

U16 code  *key_max;
U16 xdata *key_baseline;
U16 code  *key_threshold;
U16 xdata *key;
#if Touch_Adjust==1
U16 xdata *key_max_diff;
#endif
U16 xdata *key_filter[KEY_NUMBER1];
U8 	xdata *keytouch_flag;
U8 	xdata *key_flag;
U8 	xdata *baseline_sub_flag;
U8 	xdata *baseline_plus_flag;
U8 	xdata *key_count_add;
U8 	xdata *key_count_sub;
U16	xdata *DIV;  
U16 xdata  Max_delta;
U8  xdata  delta_Set;
U8 	xdata  CSI_cnt;
U8 	xdata  CSI_cnt_set;
/*===========校准配置参数============================*/
U16 xdata  upper_limit_set;
U16 xdata  lower_limit_set;
U16 xdata  temp_data;
U8 	xdata  k_temp;
bit	       TK_Adjust_Caj_flag;  //校准表示
bit	       TK_Adjust_Caj_err;   //
/*=======================================*/  
U8  xdata *R_DATA_RAM_ADDR; 
U8  xdata *C_CADJ_RAM_ADDR; 
U8  xdata *pa;
U8  xdata *pb; 
bit  tk_init_req;
bit  tk_adjust_flag,fre_switch_flag;
bit	 TK_going_flag;
bit	 TK_Scan_End_flag;
bit	 tkst_flag,tkst_flag_pre;
bit  wt_dw;
//=======================================================
// Touch Key Interrupt 
//=======================================================
void isr_TK (void) interrupt  1
{
	_push_(INSCON);
	INSCON=0X00;
	pa = R_DATA_RAM_ADDR;
	pb = C_CADJ_RAM_ADDR;
	if(TK_Adjust_Caj_flag)
	{
		Touchkey_adjust_cadj_inter();
	}
	else
	{	  
		if(TK_Scan_is_End) 				//触摸扫描结束
		{
			CLR_ALL_TK_IF;				//清触摸按键中断标志
			line++;
			if(line>=KEY_NUMBER1)
			{
				line=0;
				TK_Scan_End_flag = 1;
			}
			else
			{
				DisTk();
				EnTk(Tab_Chn[line]);
				TK_start();	 			 
			}
	  	}		
	}
	_pop_(INSCON);
}

//====================================================================
//     触摸按键信道配置表
//====================================================================
U8  code  Tab_Chn[KEY_NUMBER1] = 
{
#if KEY_NUMBER1 >= 1
  key0,
#endif
#if KEY_NUMBER1 >= 2
  key1,
#endif
#if KEY_NUMBER1 >= 3
  key2,
#endif
#if KEY_NUMBER1 >= 4
  key3,
#endif
#if KEY_NUMBER1 >= 5
  key4,
#endif
#if KEY_NUMBER1 >= 6
  key5,
#endif
#if KEY_NUMBER1 >= 7
  key6,
#endif
#if KEY_NUMBER1 >= 8
  key7,
#endif
#if KEY_NUMBER1 >= 9
  key8,
#endif
#if KEY_NUMBER1 >= 10
  key9,
#endif
#if KEY_NUMBER1 >= 11
  key10,
#endif
#if KEY_NUMBER1 >= 12
  key11,
#endif
#if KEY_NUMBER1 >= 13
  key12,
#endif
#if KEY_NUMBER1 >= 14
  key13,
#endif
#if KEY_NUMBER1 >= 15
  key14,
#endif
#if KEY_NUMBER1 >= 16
  key15,
#endif
#if KEY_NUMBER1 >= 17
  key16,
#endif
#if KEY_NUMBER1 >= 18
  key17,
#endif
#if KEY_NUMBER1 >= 19
  key18,
#endif
#if KEY_NUMBER1 >= 20
  key19,
#endif
};
//====================================================================
//   触摸按键对应最大值配置
//====================================================================
U16 code  HF_key_max[KEY_NUMBER1] = 
{
  #if KEY_NUMBER1 >= 1
    HF_diff0 ,  //key0
  #endif
  #if KEY_NUMBER1 >= 2
    HF_diff1 ,  //key1
  #endif
  #if KEY_NUMBER1 >= 3
    HF_diff2 ,  //key2
  #endif
  #if KEY_NUMBER1 >= 4
    HF_diff3 ,  //key3
  #endif
  #if KEY_NUMBER1 >= 5
    HF_diff4 ,  //key4
  #endif
  #if KEY_NUMBER1 >= 6
    HF_diff5 ,  //key5
  #endif
  #if KEY_NUMBER1 >= 7
    HF_diff6 ,  //key6
  #endif
  #if KEY_NUMBER1 >= 8
    HF_diff7 ,  //key7
  #endif
  #if KEY_NUMBER1 >= 9
    HF_diff8 ,  //key8
  #endif
  #if KEY_NUMBER1 >= 10
    HF_diff9 ,  //key9
  #endif
  #if KEY_NUMBER1 >= 11
    HF_diff10,  //key10
  #endif
  #if KEY_NUMBER1 >= 12
    HF_diff11,  //key11
  #endif
  #if KEY_NUMBER1 >= 13
    HF_diff12,  //key12
  #endif
  #if KEY_NUMBER1 >= 14
    HF_diff13,  //key13
  #endif
  #if KEY_NUMBER1 >= 15
    HF_diff14,  //key14
  #endif
  #if KEY_NUMBER1 >= 16
    HF_diff15,  //key15
  #endif
  #if KEY_NUMBER1 >= 17
    HF_diff16,  //key16
  #endif
  #if KEY_NUMBER1 >= 18
    HF_diff17,  //key17
  #endif
  #if KEY_NUMBER1 >= 19
    HF_diff18,  //key18
  #endif
  #if KEY_NUMBER1 >= 20
    HF_diff19,  //key19
  #endif
  #if KEY_NUMBER1 >= 21
    HF_diff20,  //key20
  #endif
  #if KEY_NUMBER1 >= 22
    HF_diff21,  //key21
  #endif
  #if KEY_NUMBER1 >= 23
    HF_diff22,  //key22
  #endif
  #if KEY_NUMBER1 >= 24
    HF_diff23,  //key23
  #endif
};
U16 code  LF_key_max[KEY_NUMBER1] = 
{
  #if KEY_NUMBER1 >= 1
    LF_diff0 ,  //key0
  #endif
  #if KEY_NUMBER1 >= 2
    LF_diff1 ,  //key1
  #endif
  #if KEY_NUMBER1 >= 3
    LF_diff2 ,  //key2
  #endif
  #if KEY_NUMBER1 >= 4
    LF_diff3 ,  //key3
  #endif
  #if KEY_NUMBER1 >= 5
    LF_diff4 ,  //key4
  #endif
  #if KEY_NUMBER1 >= 6
    LF_diff5 ,  //key5
  #endif
  #if KEY_NUMBER1 >= 7
    LF_diff6 ,  //key6
  #endif
  #if KEY_NUMBER1 >= 8
    LF_diff7 ,  //key7
  #endif
  #if KEY_NUMBER1 >= 9
    LF_diff8 ,  //key8
  #endif
  #if KEY_NUMBER1 >= 10
    LF_diff9 ,  //key9
  #endif
  #if KEY_NUMBER1 >= 11
    LF_diff10,  //key10
  #endif
  #if KEY_NUMBER1 >= 12
    LF_diff11,  //key11
  #endif
  #if KEY_NUMBER1 >= 13
    LF_diff12,  //key12
  #endif
  #if KEY_NUMBER1 >= 14
    LF_diff13,  //key13
  #endif
  #if KEY_NUMBER1 >= 15
    LF_diff14,  //key14
  #endif
  #if KEY_NUMBER1 >= 16
    LF_diff15,  //key15
  #endif
  #if KEY_NUMBER1 >= 17
    LF_diff16,  //key16
  #endif
  #if KEY_NUMBER1 >= 18
    LF_diff17,  //key17
  #endif
  #if KEY_NUMBER1 >= 19
    LF_diff18,  //key18
  #endif
  #if KEY_NUMBER1 >= 20
    LF_diff19,  //key19
  #endif
  #if KEY_NUMBER1 >= 21
    LF_diff20,  //key20
  #endif
  #if KEY_NUMBER1 >= 22
    LF_diff21,  //key21
  #endif
};
//====================================================================
//    TKDIV,对应的为校准电容大小
//====================================================================
U16 code  Tab_DIV[KEY_NUMBER1] = 
  {           //fre_group1
  #if KEY_NUMBER1 >= 1
    LF_DIV0,    //key_group0
  #endif
  #if KEY_NUMBER1 >= 2
    LF_DIV1,    //key_group1
  #endif
  #if KEY_NUMBER1 >= 3
    LF_DIV2,    //key_group2
  #endif
  #if KEY_NUMBER1 >= 4
    LF_DIV3,    //key_group3
  #endif
  #if KEY_NUMBER1 >= 5
    LF_DIV4,    //key_group4
  #endif
  #if KEY_NUMBER1 >= 6
    LF_DIV5,    //key_group5
  #endif
  #if KEY_NUMBER1 >= 7
    LF_DIV6,    //key_group6
  #endif
  #if KEY_NUMBER1 >= 8
    LF_DIV7,    //key_group7
  #endif
  #if KEY_NUMBER1 >= 9
    LF_DIV8,    //key_group8
  #endif
  #if KEY_NUMBER1 >= 10
    LF_DIV9,    //key_group9
  #endif
  #if KEY_NUMBER1 >= 11
    LF_DIV10,   //key_group10
  #endif
  #if KEY_NUMBER1 >= 12
    LF_DIV11,   //key_group11
  #endif
  #if KEY_NUMBER1 >= 13
    LF_DIV12,   //key_group12
  #endif
  #if KEY_NUMBER1 >= 14
    LF_DIV13,   //key_group13
  #endif
  #if KEY_NUMBER1 >= 15
    LF_DIV14,   //key_group14
  #endif
  #if KEY_NUMBER1 >= 16
    LF_DIV15,   //key_group15
  #endif
  #if KEY_NUMBER1 >= 17
    LF_DIV16,   //key_group16
  #endif
  #if KEY_NUMBER1 >= 18
    LF_DIV17,   //key_group17
  #endif
  #if KEY_NUMBER1 >= 19
    LF_DIV18,   //key_group18
  #endif
  #if KEY_NUMBER1 >= 20
    LF_DIV19,   //key_group19
  #endif
};
//=======================================================
///////////// 触摸按键阈值百分比配置表 (0~100)
////（建议根据实际触感调整该配置，从而调整各按键灵敏度）
//=======================================================
U16  code  LF_key_threshold[KEY_NUMBER1] = 
{
#if KEY_NUMBER1 >= 1
  (U32)LF_diff0 *Thrd_Per0 /100,
#endif
#if KEY_NUMBER1 >= 2
  (U32)LF_diff1 *Thrd_Per1 /100,
#endif
#if KEY_NUMBER1 >= 3
  (U32)LF_diff2 *Thrd_Per2 /100,
#endif
#if KEY_NUMBER1 >= 4
  (U32)LF_diff3 *Thrd_Per3 /100,
#endif
#if KEY_NUMBER1 >= 5
  (U32)LF_diff4 *Thrd_Per4 /100,
#endif
#if KEY_NUMBER1 >= 6
  (U32)LF_diff5 *Thrd_Per5 /100,
#endif
#if KEY_NUMBER1 >= 7
  (U32)LF_diff6 *Thrd_Per6 /100,
#endif
#if KEY_NUMBER1 >= 8
  (U32)LF_diff7 *Thrd_Per7 /100,
#endif
#if KEY_NUMBER1 >= 9
  (U32)LF_diff8 *Thrd_Per8 /100,
#endif
#if KEY_NUMBER1 >= 10
  (U32)LF_diff9 *Thrd_Per9 /100,
#endif
#if KEY_NUMBER1 >= 11
  (U32)LF_diff10 *Thrd_Per10 /100,
#endif
#if KEY_NUMBER1 >= 12
  (U32)LF_diff11 *Thrd_Per11 /100,
#endif
#if KEY_NUMBER1 >= 13
  (U32)LF_diff12 *Thrd_Per12 /100,
#endif
#if KEY_NUMBER1 >= 14
  (U32)LF_diff13 *Thrd_Per13 /100,
#endif
#if KEY_NUMBER1 >= 15
  (U32)LF_diff14 *Thrd_Per14 /100,
#endif
#if KEY_NUMBER1 >= 16
  (U32)LF_diff15 *Thrd_Per15 /100,
#endif
#if KEY_NUMBER1 >= 17
  (U32)LF_diff16 *Thrd_Per16 /100,
#endif
#if KEY_NUMBER1 >= 18
  (U32)LF_diff17 *Thrd_Per17 /100,
#endif
#if KEY_NUMBER1 >= 19
  (U32)LF_diff18 *Thrd_Per18 /100,
#endif
#if KEY_NUMBER1 >= 20
  (U32)LF_diff19 *Thrd_Per19 /100,
#endif
#if KEY_NUMBER1 >= 21
  (U32)LF_diff20 *Thrd_Per20 /100,
#endif
#if KEY_NUMBER1 >= 22
  (U32)LF_diff21 *Thrd_Per21 /100,
#endif
};


U16  code  HF_key_threshold[KEY_NUMBER1] = 
{
#if KEY_NUMBER1 >= 1
  (U32)L_H_delta *Thrd_Per0 /100,
#endif
#if KEY_NUMBER1 >= 2
  (U32)L_H_delta *Thrd_Per1 /100,
#endif
#if KEY_NUMBER1 >= 3
  (U32)L_H_delta *Thrd_Per2 /100,
#endif
#if KEY_NUMBER1 >= 4
  (U32)L_H_delta *Thrd_Per3 /100,
#endif
#if KEY_NUMBER1 >= 5
  (U32)L_H_delta *Thrd_Per4 /100,
#endif
#if KEY_NUMBER1 >= 6
  (U32)L_H_delta *Thrd_Per5 /100,
#endif
#if KEY_NUMBER1 >= 7
  (U32)L_H_delta *Thrd_Per6 /100,
#endif
#if KEY_NUMBER1 >= 8
  (U32)L_H_delta *Thrd_Per7 /100,
#endif
#if KEY_NUMBER1 >= 9
  (U32)L_H_delta *Thrd_Per8 /100,
#endif
#if KEY_NUMBER1 >= 10
  (U32)L_H_delta *Thrd_Per9 /100,
#endif
#if KEY_NUMBER1 >= 11
  (U32)L_H_delta *Thrd_Per10 /100,
#endif
#if KEY_NUMBER1 >= 12
  (U32)L_H_delta *Thrd_Per11 /100,
#endif
#if KEY_NUMBER1 >= 13
  (U32)L_H_delta *Thrd_Per12 /100,
#endif
#if KEY_NUMBER1 >= 14
  (U32)L_H_delta *Thrd_Per13 /100,
#endif
#if KEY_NUMBER1 >= 15
  (U32)L_H_delta *Thrd_Per14 /100,
#endif
#if KEY_NUMBER1 >= 16
  (U32)L_H_delta *Thrd_Per15 /100,
#endif
#if KEY_NUMBER1 >= 17
  (U32)L_H_delta *Thrd_Per16 /100,
#endif
#if KEY_NUMBER1 >= 18
  (U32)L_H_delta *Thrd_Per17 /100,
#endif
#if KEY_NUMBER1 >= 19
  (U32)L_H_delta *Thrd_Per18 /100,
#endif
#if KEY_NUMBER1 >= 20
  (U32)L_H_delta *Thrd_Per19 /100,
#endif
#if KEY_NUMBER1 >= 21
  (U32)L_H_delta *Thrd_Per20 /100,
#endif
#if KEY_NUMBER1 >= 22
  (U32)L_H_delta *Thrd_Per21 /100,
#endif
};
//-------------------------------------------------------------------
//
//-------------------------------------------------------------------
U8  code  key_div_per[KEY_NUMBER1] = 
{
#if KEY_NUMBER1 >= 1
 L_H_delta*16/HF_diff0,  //
#endif
#if KEY_NUMBER1 >= 2
 L_H_delta*16/HF_diff1,  //
#endif
#if KEY_NUMBER1 >= 3
 L_H_delta*16/HF_diff2,  //
#endif
#if KEY_NUMBER1 >= 4
 L_H_delta*16/HF_diff3,  //
#endif

#if KEY_NUMBER1 >= 5
 L_H_delta*16/HF_diff4,  //
#endif
#if KEY_NUMBER1 >= 6
 L_H_delta*16/HF_diff5,  //
#endif
#if KEY_NUMBER1 >= 7
 L_H_delta*16/HF_diff6,  //
#endif
#if KEY_NUMBER1 >= 8
 L_H_delta*16/HF_diff7,  //
#endif

#if KEY_NUMBER1 >= 9
 L_H_delta*16/HF_diff8,  //
#endif
#if KEY_NUMBER1 >= 10
 L_H_delta*16/HF_diff9,  //
#endif
#if KEY_NUMBER1 >= 11
 L_H_delta*16/HF_diff10,  //
#endif
#if KEY_NUMBER1 >= 12
 L_H_delta*16/HF_diff11,  //
#endif

#if KEY_NUMBER1 >= 13
 L_H_delta*16/HF_diff12,  //
#endif
#if KEY_NUMBER1 >= 14
 L_H_delta*16/HF_diff13,  //
#endif
#if KEY_NUMBER1 >= 15
 L_H_delta*16/HF_diff14,  //
#endif
#if KEY_NUMBER1 >= 16
 L_H_delta*16/HF_diff15,  //
#endif

#if KEY_NUMBER1 >= 17
 L_H_delta*16/HF_diff16,  //
#endif
#if KEY_NUMBER1 >= 18
 L_H_delta*16/HF_diff17,  //
#endif
#if KEY_NUMBER1 >= 19
 L_H_delta*16/HF_diff18,  //
#endif

#if KEY_NUMBER1 >= 20
 L_H_delta*16/HF_diff19,  //
#endif
#if KEY_NUMBER1 >= 21
 L_H_delta*16/HF_diff20,  //
#endif
};
//====================================================================
// 触摸按键初始化
//====================================================================
void tk_para_init()
{
	tk_num 				= 	KEY_NUMBER1;
	Add_Set 			= 	OtherSet;			
	Add_function 		= 	Function;
	FLAG_COUNT_set 		= 	FLAG_COUNT;
	init_BaseCnt_set 	= 	L_BaseCnt_set;
	
	R_DATA_RAM_ADDR 	= 	TK_DATA_RAM_ADDR;
	C_CADJ_RAM_ADDR 	= 	TK_CADJ_RAM_ADDR;
	Frequcecy_div_set   =	L_Frequcecy_div;
	Frequcecy_filter_set =  L_Frequcecy_filter;
#if ((Function&0x01) ==0x01)
	R_water_add_count = 0;
	R_water_Delay = L_WT_Delay;
	R_water_num = multikey_num;
	R_Inter1  = L_Inter1;
#endif
	key_fastcounter   	= 	key_fastcounter_set ;
	R_downCnt    		= 	L_downCnt;
	key_WT_set 			= 	L_key_WT_set;
	R_Cnt_WT            =   L_Cnt_WT;
	delta_Set  			= 	key_WT_set&0xf;
	Fre_switch_Set      = 	L_wt_Speed;
	R_HF_MaxLimit       =   L_H_delta_max;
	multikey_control    =   multikey_num;
	R_data_add          =   data_add;
	CSI_cnt_set         =   L_CSI_cnt_set;
	tk_init_req 		= 	1;
	line				=	0;
    sino_key_value 		= 	0;
	key_sample_count    =   0;
	TK_HF_initSet();

	tkst_flag 		    = 	TKST_IS_High_Fre;
	tkst_flag_pre 	    = 	TKST_IS_High_Fre;
	touchkey_samp_fre_set();
	
    IEN0 |= 0x02;
    Enable_Touch;
	LF_key_threshold2 = (U32)LF_diff0*L_Pre_FT/100;
	LF_key_threshold3 = (U32)LF_diff0*L_Pre_WT/100;

}   

//====================================================================
// 触摸按键
//====================================================================
void TK_HF_initSet()
{
U8 i;
	DisTk();
	for(i=0;i<KEY_NUMBER1;i++)
	{			
		EnIO(Tab_Chn[i]);
	}
	EnTk(Tab_Chn[0]);
	TKCON1 	= TKCON_msg;
	TKCLK  	= TKCLK_msg;

	TKSTEP1  = TKstep_Fre_Hmsg;				
	TKSTEP2  = TKstep_Fre_Hmsg;
	TKSTEP3  = TKstep_Fre_H_step3_6;
	TKSTEP4  = TKstep_Fre_Hmsg;
	TKSTEP5  = TKstep_Fre_Hmsg;
	TKSTEP6  = TKstep_Fre_H_step3_6;
	TKSTEP7  = TKSTEP7_msg;
	TKADCT   = TKADCT_msg;
	TKMDC  	 = TKMDC_msg;
	TKRANDOM = TKRANDOM_msg;
	TKPARA   = TKPARA_msg;
	TKCFL    = TKCFL_msg;	
}
//====================================================================
// 触摸按键扫描信道开启
//====================================================================
void EnTk(U8 n)
{	
	switch(n)
	{
		case 0:		P0SS 	|= BIT0;	TKU1 |= BIT0;		break;
		case 1:		P0SS 	|= BIT1;	TKU1 |= BIT1;		break;
		case 2:		P0SS 	|= BIT2;	TKU1 |= BIT2;		break;
		case 3:		P0SS 	|= BIT3;	TKU1 |= BIT3;		break;
		case 4:		P0SS 	|= BIT4;	TKU1 |= BIT4;		break;
		case 5:		P0SS 	|= BIT5;	TKU1 |= BIT5;		break;
		case 6:		P0SS 	|= BIT6;	TKU1 |= BIT6;		break;
		case 7:		P0SS 	|= BIT7;	TKU1 |= BIT7;		break;
		case 8:     P1SS    |= BIT0;    TKU2 |=	BIT0;		break;
		case 9:     P1SS    |= BIT1;    TKU2 |=	BIT1;		break;
		case 10:    P1SS    |= BIT2;    TKU2 |=	BIT2;		break;
		case 11:    P1SS    |= BIT3;    TKU2 |=	BIT3;		break;
		case 12:    P1SS    |= BIT4;    TKU2 |=	BIT4;		break;
		case 13:    P1SS    |= BIT5;    TKU2 |=	BIT5;		break;
		case 14:    P1SS    |= BIT6;    TKU2 |=	BIT6;		break;
		case 15:    P1SS    |= BIT7;    TKU2 |=	BIT7;		break;
		case 16:    P2SS    |= BIT2;    TKU3 |=	BIT0;		break;
		case 17:    P2SS    |= BIT3;    TKU3 |=	BIT1;		break;
		case 18:    P2SS    |= BIT4;    TKU3 |=	BIT2;		break;
		case 19:    P2SS    |= BIT5;    TKU3 |=	BIT3;		break;
		case 20:	TKPARA  |= BIT7;						break;	
	}
}
//====================================================================
// 触摸按键扫描信道IO设置
//====================================================================
void EnIO(U8 n)
{	
	switch(n)
	{
		case 0:		P2CR	|= BIT7;	P2 &= 0x7F;		    break;
		case 1:		P3CR 	|= BIT0;	P3 &= 0xFE;		    break;
		case 2:		P0CR 	|= BIT0;	P0 &= 0xFE;		    break;
		case 3:		P0CR 	|= BIT3;	P0 &= 0xF7;		    break;
		case 4:		P0CR 	|= BIT4;	P0 &= 0xEF;		    break;
		case 5:		P0CR	|= BIT5;	P0 &= 0xDF;		    break;
		case 6:		P0CR 	|= BIT6;	P0 &= 0xBF;		    break;
		case 7:		P0CR 	|= BIT7;	P0 &= 0x7F;	     	break;
	    case 8:     P1CR    |= BIT0;    P1 &= 0xFE;	    	break;
		case 9:     P1CR    |= BIT1;    P1 &= 0xFD; 		break;
		case 10:    P1CR    |= BIT2;    P1 &= 0xFB; 		break;
		case 11:    P1CR    |= BIT3;    P1 &= 0xF7; 		break;
		case 12:    P1CR    |= BIT4;    P1 &= 0xEF; 		break;
		case 13:    P1CR    |= BIT5;    P1 &= 0xDF;			break;
		case 14:    P1CR    |= BIT6;    P1 &= 0xBF;	    	break;
		case 15:    P1CR    |= BIT7;    P1 &= 0x7F;	    	break;
		case 16:    P2CR    |= BIT2;    P2 &= 0xFB; 		break;
		case 17:    P2CR    |= BIT3;    P2 &= 0xF7; 		break;
		case 18:    P2CR    |= BIT4;    P2 &= 0xEF; 		break;
		case 19:    P2CR    |= BIT5;    P2 &= 0xDF; 		break;
		default:											break;
	}
}

//====================================================================
// Disable touch key 
//====================================================================
void DisTk(void)
 {
   TKU1=0;
   TKU2=0;
   TKU3=0;
   P0SS=0;
   P1SS=0;
   P2SS=0;
   TKPARA  &= 0x7F;  // 关闭假键
}
//=======================================================
//
//=======================================================
void TK_start()
{
	TK_GO;
}
//=======================================================
//
//=======================================================
#if Touch_Adjust ==1
void Rst_WDT()
{
	RSET_WATCHDOG;
}
#endif
//=======================================================
//	 校准程序参数初始化
//=======================================================
void Touchkey_adjust()
{
	TK_Adjust_Caj_flag = 1;
	TK_Adjust_Caj_err  = 0;
    tk_num  =  KEY_NUMBER1;
	R_TKstep_Fre_Lmsg 		= TKstep_Fre_Lmsg;
	R_TKstep_Fre_Hmsg 		= TKstep_Fre_Hmsg;
	R_TKstep_Fre_H_step3_6 	= TKstep_Fre_H_step3_6;
	R_Fre_ST = 1;

	get_cs_data();
	if(TK_Adjust_Caj_err == 1)
	{
	   TK_Adjust_Caj_flag = 1;
	   get_cs_data();
	}
}
//=======================================================
//	 
//=======================================================
void get_cs_data()
{
U8 i;
	    TK_HF_initSet();
		TKCON1 	 = 0x04;	  //校准CS使用18次采样
		R_DATA_RAM_ADDR = TK_DATA_RAM_ADDR;
		C_CADJ_RAM_ADDR = TK_CADJ_RAM_ADDR;
		upper_limit_set = upper_limit;
		lower_limit_set = lower_limit;
		Enable_Touch;
		IEN0 |= 0x02;
		line  = 0;
		Touchkey_adjust_cadj();
		TK_Scan_End_flag = 0;
		TK_going_flag = 0;	   //标志TK空闲状态
		TK_start();
		while(TK_Adjust_Caj_flag)
		{
		   RSTSTAT = 0;
		}	
	//---------------------------------------------
		for(i=0;i<tk_num;i++)
		{			
			HF_DIV[i] =  LF_DIV[i];
		}
		TKSTEP1  = TKstep_Fre_Lmsg;
		TKSTEP2  = TKstep_Fre_Lmsg;
		TKSTEP3  = TKstep_Fre_Lmsg;
		TKSTEP4  = TKstep_Fre_Lmsg;
		TKSTEP5  = TKstep_Fre_Lmsg;
		TKSTEP6  = TKstep_Fre_Lmsg;
		DisTk();
		EnTk(Tab_Chn[0]);
		TK_Adjust_Caj_flag =1;
		line = 0;
		Touchkey_adjust_cadj();
		TK_Scan_End_flag = 0;
		TK_going_flag = 0;	   //标志TK空闲状态
		TKCON1 |= 0x80;
		TK_start();
		while(TK_Adjust_Caj_flag)
		{
		   RSTSTAT = 0;
		}
}
 //====================================================================
//
// 触摸按键开关频率及放大系数设定
//
//====================================================================
void tk_samp_fre_set()	
{
	DisTk();
	EnTk(Tab_Chn[0]);
	#if L_wt_Speed == 0xff
	   key_sample_count++;	 
	#endif	
	
	tk_fre_update();		
}
//=====================================================================
//
//=====================================================================
void touchkey_reset_chk()
{
U16 rwk0;
U8 i;
    if(Add_Set&0x02)
	{
	   rwk0 = STcnt_set;		 
	   if(sino_key_value&Long_key)   
		  rwk0 = STcnt_set2;	 

       if(R_Reset_Cnt > rwk0)
	   {
		 	R_Reset_Cnt =0;
		    for(i=0;i<tk_num;i++)
			{	
	 			 LF_key_baseline[i] =  LF_key[i];  
			}	
	   }
       if(R_Reset_Cnt_H > rwk0)
	   {
		 	R_Reset_Cnt_H =0;
		    for(i=0;i<tk_num;i++)
			{	
	 			 HF_key_baseline[i] =  HF_key[i];  
			}		 	
	   }	        	    
	}
 	#if ((Function&0x01) ==0x01)
	   touchkey_rschk();			  
	#endif		    
}
//=====================================================================
//
//=====================================================================
void Get_TK_States()
{
	touchkey_go();
	if(TK_Scan_End_flag)
	{
        TK_going_flag    = 0;        
        TK_Scan_End_flag = 0; 
		              
        GetTouchkeyData();
		key_judge();  
		touchkey_go();   	
	}		
}
//=======================================================
//	
//=======================================================
#if ((Function&0x01) ==0x01)
void touchkey_rschk()
{
U8 R_TK_WT_NUM;
U8 i;
U16 rwk0;
    R_TK_WT_NUM = 0;
	for(i=0;i<tk_num;i++)
	{
	  if(LF_key[i] > LF_key_baseline[i])
		 rwk0 = LF_key[i] - LF_key_baseline[i];
	  else
	     rwk0 = LF_key_baseline[i] - LF_key[i];

	  if(rwk0 > ((LF_key_threshold[i]*R_Inter1 )>>4))	//
		  R_TK_WT_NUM++;  
	 
	}
	if(R_TK_WT_NUM > R_water_num)
	{
	   R_water_add_count = R_water_Delay;	//
	   for(i=0;i<tk_num;i++)
	   {
	      LF_key_baseline[i] = LF_key[i];
		  HF_key_baseline[i] = HF_key[i];
	   }	  	
	}
	if(R_water_add_count!=0)
	{
	   R_water_add_count--;
	   if(R_water_add_count ==0)
	   {
		   for(i=0;i<tk_num;i++)
		   {
		      LF_key_baseline[i] = LF_key[i];
			  HF_key_baseline[i] = HF_key[i];
		   }	   		
	   }
	   sino_key_value = 0;
	}			 			
}
#endif
//=======================================================
//	上电自检程序
//=======================================================
#if Check_KeyChl ==1
U32 xdata  sns_short_flag=0;	//
U32	xdata  sns_open_flag=0;		 //
U32 xdata  sns_Pseudo_Soldering_flag = 0; //
void sns_self_check()
{
U8 i;	
U8 chn_temp =0;
U16 temp_cs_div = 0;
U16 cs_diff = 0;						 
   for(i=0;i<tk_num;i++)
   {
  		if((LF_DIV[i]<150)||(LF_DIV[i]>2000))	//
		{
			sns_short_flag |= ((U32)0x0001 << i);
		}
   }		                                          
   for(i=0;i<tk_num;i++)				       //
   {
   	  	chn_temp = Tab_Chn[line];			 
	    if(((chn_temp > 14)&&(LF_DIV[i]<200))||((chn_temp <=14)&&(LF_DIV[i]<100)))	//
	    {
		    sns_open_flag	|= ((U32)0x0001 << i);	// 
	    }

	   temp_cs_div = Tab_DIV[i];		  			//   
	   if(LF_DIV[i]>temp_cs_div)
	   	  cs_diff = LF_DIV[i] - temp_cs_div;	
	   else
	   	  cs_diff = temp_cs_div - LF_DIV[i];
	   if(cs_diff > LF_DIV[i] >> 2)    //
	  	   sns_Pseudo_Soldering_flag |= ((U32)0x0001 << i);
   }	
}
#endif
///*----------------------------------------------------*/
///*          1.  按键个数参数设置错误                  */
///*----------------------------------------------------*/
#if(KEY_NUMBER1 <  multikey_num)
	#error"按键总數小于多按键數,请在配置界面(tk_conf_add.h)中修改按键总个數或多按键个數"
#endif
