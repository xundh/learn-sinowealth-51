#ifndef __TK_CONF_H
#define __TK_CONF_H

#define KEY_NUMBER        20  //按键总个数，取值范围（1~24）
#define Key_Group_Num     20  //按键分组数，取值范围（1~KEY_NUMBER）
//
#define HF_TKST0          0
#define HF_TKST1          0
#define HF_TKST2          0
#define HF_TKST3          0
#define HF_TKST4          0
#define HF_TKST5          0
#define HF_TKST6          0
#define HF_TKST7          0
#define HF_TKST8          0
#define HF_TKST9          0
#define HF_TKST10         0
#define HF_TKST11         0
#define HF_TKST12         0
#define HF_TKST13         0
#define HF_TKST14         0
#define HF_TKST15         0
#define HF_TKST16         0
#define HF_TKST17         0
#define HF_TKST18         0
#define HF_TKST19         0

#define HF_DIV0           200
#define HF_DIV1           200
#define HF_DIV2           200
#define HF_DIV3           200
#define HF_DIV4           200
#define HF_DIV5           200
#define HF_DIV6           200
#define HF_DIV7           200
#define HF_DIV8           200
#define HF_DIV9           200
#define HF_DIV10          200
#define HF_DIV11          200
#define HF_DIV12          200
#define HF_DIV13          200
#define HF_DIV14          200
#define HF_DIV15          200
#define HF_DIV16          200
#define HF_DIV17          200
#define HF_DIV18          200
#define HF_DIV19          200


#define HF_diff0          300
#define HF_diff1          300
#define HF_diff2          300
#define HF_diff3          300
#define HF_diff4          300
#define HF_diff5          300
#define HF_diff6          300
#define HF_diff7          300
#define HF_diff8          300
#define HF_diff9          300
#define HF_diff10         300
#define HF_diff11         300
#define HF_diff12         300
#define HF_diff13         300
#define HF_diff14         300
#define HF_diff15         300
#define HF_diff16         300
#define HF_diff17         300
#define HF_diff18         300
#define HF_diff19         300

#define LF_TKST0          24
#define LF_TKST1          24
#define LF_TKST2          24
#define LF_TKST3          24
#define LF_TKST4          24
#define LF_TKST5          24
#define LF_TKST6          24
#define LF_TKST7          24
#define LF_TKST8          24
#define LF_TKST9          24
#define LF_TKST10         24
#define LF_TKST11         24
#define LF_TKST12         24
#define LF_TKST13         24
#define LF_TKST14         24
#define LF_TKST15         24
#define LF_TKST16         24
#define LF_TKST17         24
#define LF_TKST18         24
#define LF_TKST19         24

#define LF_DIV0           200
#define LF_DIV1           200
#define LF_DIV2           200
#define LF_DIV3           200
#define LF_DIV4           200
#define LF_DIV5           200
#define LF_DIV6           200
#define LF_DIV7           200
#define LF_DIV8           200
#define LF_DIV9           200
#define LF_DIV10          200
#define LF_DIV11          200
#define LF_DIV12          200
#define LF_DIV13          200
#define LF_DIV14          200
#define LF_DIV15          200
#define LF_DIV16          200
#define LF_DIV17          200
#define LF_DIV18          200
#define LF_DIV19          200

#define LF_diff0          400
#define LF_diff1          400
#define LF_diff2          400
#define LF_diff3          400
#define LF_diff4          400
#define LF_diff5          400
#define LF_diff6          400
#define LF_diff7          400
#define LF_diff8          400
#define LF_diff9          400
#define LF_diff10         400
#define LF_diff11         400
#define LF_diff12         400
#define LF_diff13         400
#define LF_diff14         400
#define LF_diff15         400
#define LF_diff16         400
#define LF_diff17         400
#define LF_diff18         400
#define LF_diff19         400

#endif



