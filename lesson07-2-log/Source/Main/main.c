#include "SH79F9476.h"
#include "clk_utils.h"
#include "cpu.h"
#include "common_utils.h"
#include "isr_utils.h"
#include "log_utils.h"
#include <stdio.h>

// 发送缓冲区
void main() {
    char index = 0x31;
    // 选择高速时钟
    highFrequenceClk();

    enableAllIsr();
    // 初始化串口
    Uart0_Init();

    while (1) {
        LOGI((TAG, "info(%bd) = %c",index, index));
        delay_ms(100);
        LOGE((TAG, "err(%bd) = %c",index, index));

        index++;
        if (index > 0x7d)index = 0x31;

        // 暂停
        delay_ms(100);
    }
}
