#include "adc_utils.h"


/**
 * @brief ADC的单一通道转换（AN0）。
 *
 * 配置ADC使用VDD作为电压参考，没有触发。
 * 系统时钟设置为24M，ADC设置为50ksps采样。
 * 软件开始ADC转换，结果存储在ADC_res数组中。
 */
void init_adc_one(void){
    CLKCON = 0X00;  // 设置时钟控制寄存器
    ADCON1 = 0x80;	// 设置ADC控制寄存器1（ADON，VDD作为vref，没有触发）

    // 设置ADC时钟周期以实现24M SYSCLK下的50ksps
    ADT=0x95;

    ADCON2 = 0x00;	// 设置ADC控制寄存器2为1通道
    ADCH1 = 0x01;	// 设置ADC通道选择寄存器1为AN0
    SEQCHX = 0x00;	// 设置ADC序列控制寄存器为CH0

}
/**
 * @brief ADC的单一通道转换（AN0）。
 * @return
 */
UINT16 adc_one_trans(void){
    UINT16 result;
    // 启动ADC转换
    ADCON1 |= 0x01;  // 设置ADC GO位
    while(ADCON1 & 0x01);	// 等待ADC转换完成（检查go/done位）
    result = ((ADDXH << 4) + (ADDXL >> 4)); // 在ADC_res数组中存储ADC结果
    return result;
}
void init_adc_array(void){
    CLKCON = 0X00;      // 设置时钟控制寄存器
    ADCON1 = 0x80;	    // 设置ADC控制寄存器1（ADON，VDD作为vref，没有触发）

    // 24M SYSCLK, 50ksps
    ADT=0x95;

    ADCON2 = 0x72;	    // 8通道，间隔时间：4Tad
    ADCH1 = 0xff;	    // P0.7~P0.0,AN7~AN0通道作为adc输入
    // 配置通道交易顺序：AN7,AN6, AN5, AN4, AN3, AN2, AN1, AN0
    SEQCON = 0x00;	    // 配置SEQCH0
    SEQCHX = 0x07;	    // CH7
    SEQCON = 0x01;	    // 配置SEQCH1
    SEQCHX = 0x06;	    // CH6
    SEQCON = 0x02;	    // 配置SEQCH2
    SEQCHX = 0x05;	    // CH5
    SEQCON = 0x03;	    // 配置SEQCH3
    SEQCHX = 0x04;	    // CH4
    SEQCON = 0x04;	    // 配置SEQCH4
    SEQCHX = 0x03;	    // CH3
    SEQCON = 0x05;	    // 配置SEQCH5
    SEQCHX = 0x02;	    // CH2
    SEQCON = 0x06;	    // 配置SEQCH6
    SEQCHX = 0x01;	    // CH1
    SEQCON = 0x07;	    // 配置SEQCH7
    SEQCHX = 0x00;	    // CH0
    SEQCON &= 0x7f;	    // 结果左对齐
}
/**
 * @brief ADC的多通道转换（AN0~AN7）。
 */
void adc_array_trans(void){
    UCHAR i=0;
    UINT16 ADC_res[8];

    // 启动AD转换
    ADCON1 |= 0x01;
    // 检查go/done
    while(ADCON1 & 0x01);
    // 获取结果。
    for(i = 0; i < 8; i++)
    {
        SEQCON = i; // 设置SEQCON为当前索引
        ADC_res[i] = ((ADDXH << 4) + (ADDXL >> 4)); // 在ADC_res数组中存储ADC结果
    }
}

/**
 * @brief ADC中断处理函数
 */
void INT_ADC(void) interrupt 6
{
    _push_(INSCON);
    select_bank0();

    // 清除ADCON1寄存器的第6位(完成ADC的中断标志位) 1011 1111
    ADCON1 &=0xbf;

    _pop_(INSCON);
}
