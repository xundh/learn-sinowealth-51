#ifndef __UEART_UTILS_H__
#define __UEART_UTILS_H__

#include "cpu.h"

#define UART0_DATA_BUF_SIZE 60

/**
* @brief 初始化串口
*/
void Uart0_Init();

/**
 * @brief 发送缓冲区数据
 */
void Uart0_Transmit(U8 len);
/**
* @brief 发送一个字节
*/
void Uart0_Send_Byte(U8 byte);
/**
 * @brief 发送数组
 * @param bytes
 * @param len
 */
void Uart0_Send_Bytes(const U8* bytes, U8 len);
/**
 * @brief 发送字符串
 * @param str
 * @param len
 */
void Uart0_Send_String(const char *str);
#endif
