#include "pca0_utils.h"

/**
* @brief PCA0 工作方式0 边沿触发
*/
void PCA0Mode0(void){
  select_bank1();
  // 工作在单沿模式、禁止PCA0溢出中断、选择系统时钟作为PCA0时钟源
  P0CMD=0x00;
  // 捕捉方式、任意沿触发、打开捕捉/比较中断、使能比较/捕捉模块0
  P0CPM0=0x39;
  // 配置计数器最大值
  P0TOPH=0x08;
  P0TOPL=0x00;
  // 启动PCA0计数器
  PCACON=0x1;
  select_bank0();
}
/**
* @brief PCA0 工作方式1 软件定时器
*/
void PCA0Mode1(void){
  select_bank1();
  // 单沿模式、禁止PCA0溢出中断、选择系统时钟作为PCA0时钟源
  P0CMD = 0x00;
  // 连续软件定时方式、允许P0CEX0输出波形、打开捕捉/比较中断
  P0CPM0 = 0x47;
  // 配置PCA0 计数器最大值
  P0TOPH = 0x40;
  P0TOPL = 0x20;
  // 配置比较/捕捉模块匹配值
  P0CPH0 = 0x00;
  P0CPL0 = 0x20;
  // 使能比较/捕捉模块
  P0CPM0 |= 0x08;
  // 启动PCA0计数器
  PCACON = 0x1;
  select_bank0();
}
/**
* @brief PCA0 工作方式2 频率输出方式
*/
void PCA0Mode2(void){
  select_bank1();
  // 单沿模式、允许PCA0溢出中断、选择系统时钟作为PCA0时钟源
  P0CMD = 0x80;
  // 频率输出方式、允许P0CEX0输出波形
  P0CPM0 = 0x80;
  // 配置比较/捕捉模块匹配值
  P0CPH0 = 0x10;
  P0CPL0 = 0x80;
	//P0TOPH = 0x40;
  // 使能比较/捕捉模块、启动PCA0计数器
  PCACON = 0x1;
  P0CPM0 |= 0x08;
  select_bank0();
}
/**
* @brief PCA0 工作方式3 PWM8模式
*/
void PCA0Mode3PWM8(void){
  select_bank1();
  // 单沿模式、允许PCA0溢出中断、选择系统时钟作为PCA0时钟源
  P0CMD = 0x80;

  // P0CM0 配置比较/捕捉模块0工作方式：PWM8模式、打开捕捉/比较中断
  P0CPM0 = 0xC3;
  // 配置比较/捕捉模块0匹配值
  P0CPH0 = 0x30;
  P0CPL0 = 0x30;
	P0TOPH = 0x80;
  // 使能比较/捕捉模块、PWM正向输出波形
  P0CPM0 |= 0x08;

  // P0CPM1 配置比较/捕捉模块1工作方式： PWM输出方式，打开捕捉/比较中断
  P0CPM1 = 0xC3;
  // 配置比较/捕捉模块1匹配值
  P0CPH1 = 0x30;
  P0CPL1 = 0x30;
  // 使能比较/捕捉模块1、PWM反向输出波形
  P0CPM1 |= 0x0C;
  
  // 启动PCA0计数器
  PCACON = 0x01;
  select_bank0();
}
/**
* @brief PCA0 工作方式3 PWM16模式
*/
void PCA0Mode3PWM16(void){
  select_bank1();
  // 单沿模式、允许PCA0溢出中断、选择系统时钟作为PCA0时钟源
	P0CMD=0x80;
  // P0CPM0 配置比较/捕捉模块0工作方式：PWM16模式、打开捕捉/比较中断
	P0CPM0=0xD3;
  // 配置比较/捕捉模块0匹配值
	P0CPH0=0x30;
	P0CPL0=0x30;
	// 使能比较/捕捉模块0、PWM正向输出波形
	P0CPM0|=0x08;
	
  // P0CPM1 配置比较/捕捉模块1工作方式：PWM16模式，打开捕捉/比较中断
	P0CPM1=0xD3;
  // 配置比较/捕捉模块1匹配值
	P0CPH1=0x30;
	P0CPL1=0x30;
  // 使能比较/捕捉模块1、PWM反向输出波形
	P0CPM1|=0x0c;

  // 启动PCA0计数器
	PCACON=0x1;
  select_bank0();
}
/**
* @brief PCA0 工作方式3 XPWM16模式
*/
void PCA0Mode3XPWM16(void){
  select_bank1();
  // 双沿模式、允许PCA0溢出中断、选择系统时钟作为PCA0时钟源
	P0CMD=0xC0;
  // 配置PCA0计数最大值
	P0TOPH=0x01;
	P0TOPL=0x00;

  // P0CPM0 配置比较/捕捉模块0工作方式：16位相位修正PWM输出方式、打开捕捉/比较中断
	P0CPM0=0xE3;
  // 配置比较/捕捉模块0匹配值
	P0CPH0=0x00;
	P0CPL0=0x90;
  // 使能比较/捕捉模块0、PWM正向输出波形
	P0CPM0|=0x08;

  // P0CPM1 配置比较/捕捉模块1工作方式：16位相位修正PWM输出方式，打开捕捉/比较中断
	P0CPM1=0xE3;
  // 配置比较/捕捉模块1匹配值
	P0CPH1=0x0;
	P0CPL1=0x90;
  // 使能比较/捕捉模块1、PWM反向输出波形
	P0CPM1|=0x0c;
  
  // 启动PCA0计数器
	PCACON=0x1;
  select_bank0();
}
/**
* @brief PCA0中断
*/
void INT_PCA0(void) interrupt 20
{ 
  _push_(INSCON);
  Select_Bank1();
	// 清除溢出标志
  P0CF = 0x00;
  _pop_(INSCON);       
}
