#include "clk_utils.h"
/**
* @brief 高速时钟模式
*/
void highFrequenceClk(void){
	  CLKCON =0x08;
	  Delay();
	  CLKCON|=0x04;
}
/**
* @brief 低速时钟模式
*/
void lowFrequenceClk(void){
    // 设置FS为0，选择OSC1CLK为系统时钟
    CLKCON &=0xFB;	  
    _nop_();
    // 关闭高频时钟OSC2CLK
    CLKCON &=0xF7;      
    _nop_();
    _nop_();
    _nop_();
    _nop_();
    // 关闭分频  Fsys=Fosc
    CLKCON &=0x9F;	  
}
