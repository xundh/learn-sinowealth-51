#ifndef __CLK_UTIL_H__
#define __CLK_UTIL_H__
#include "SH79F9476.h"
#include "intrins.h"
#include "api_ext.h"
/**
* @brief 高速时钟模式
*/
void highFrequenceClk(void);
/**
* @brief 低速时钟模式
*/
void lowFrequenceClk(void);

#endif
