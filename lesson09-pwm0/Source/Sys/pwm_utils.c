#include "pwm_utils.h"

/**
 * @param frequency 频率
 * @param duty 占空比,单位是百分比
 * @param polar 极性
 * @brief 初始化PWM0
 */
void Pwm0_Init(volatile U16 frequency, volatile U16 duty, volatile U16 polar) {
    // 周期，单位是ms
    volatile U16 period = 1000 / frequency;
    volatile U16 PWM0P = period * 128;
    volatile U16 PWM0D = (PWM0P / 100) * duty;
    _push_(INSCON);
    Select_Bank0();
    // 周期寄存器， 0x0fff=4095，周期=4095/128K=32ms
    // 通过周期计算PWM0Px的值，PWM0Px=period*128K/1000
    PWM0PL = PWM0P & 0xff;
    PWM0PH = PWM0P >> 8;
    // 占空比寄存器,0x07ff=2047
    // 计算占空比寄存器的值，PWM0Dx=周期寄存器值*duty/100
    PWM0DL = PWM0D & 0xff;
    PWM0DH = PWM0D >> 8;

    if (polar == 1) {
        PWM0CON |= 0x40;
    } else {
        PWM0CON &= 0xbf;
    }
    // PWM0控制寄存器 1000_0001, 使能、输出允许
    PWM0CON |= 0x81;
    _pop_(INSCON);
}

/**
 * @param frequency 频率
 * @param duty 占空比,单位是百分比
 * @param polar 极性
 * @brief 初始化PWM1
 */
void Pwm1_Init(volatile U16 frequency, volatile U16 duty, volatile U16 polar) {
    // 周期，单位是ms
    volatile U16 period = 1000 / frequency;
    volatile U16 PWM1P = period * 128;
    volatile U16 PWM1D = (PWM1P / 100) * duty;
    _push_(INSCON);
    Select_Bank1();
    // 周期寄存器
    PWM1PL = PWM1P & 0xff;
    PWM1PH = PWM1P >> 8;
    // 占空比寄存器
    PWM1DL = PWM1D & 0xff;
    PWM1DH = PWM1D >> 8;

    if (polar == 1) {
        PWM1CON |= 0x40;
    } else {
        PWM1CON &= 0xbf;
    }
    // PWM1控制寄存器 1100_0001，使能、占空比期间输出低电平、占空比溢出后输出高电平、输出允许
    PWM1CON |= 0x81;
    _pop_(INSCON);
}
