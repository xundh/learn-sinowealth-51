#include "SH79F9476.h"
#include "clk_utils.h"
#include "cpu.h"
#include "euart_utils.h"
#include "common_utils.h"
#include <stdio.h>


void main() {
		char index=0x31;
    // 选择高速时钟
    highFrequenceClk();

	
    // 初始化串口
    Uart0_Init();


    while (1) {
        printf("char(%bd) = %c \n",index, index);
				index++;
				if(index>0x7d)index=0x31;
			
        // 暂停
        delay_ms(500);
    }
}
