//
// Created by hs26661 on 2024/4/19.
//
#include "common_utils.h"


void delay_us(void) {
    UINT16 i;
    for (i = 0; i < 1000; i++);
}

void delay_ms(U16 ms) {
    UINT16 i;
    for (i = 0; i < ms; i++)
        delay_us();
}