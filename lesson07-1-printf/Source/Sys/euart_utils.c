#include "intrins.h"
#include "euart_utils.h"
#include "api_ext.h"
#include "SH79F9476.h"
#include "cpu.h"
#include "string.h"

// 发送缓冲区
U8 gUart0DataTxD[UART0_DATA_BUF_SIZE];
// 未发送数据长度
U8 gUart0Len_TxD = 0;

// 内部变量，发送指针
volatile U8 *ptr_tx0;

/**
* @brief 初始化串口
*/
void Uart0_Init() {
    //=====TX 建议配置为输出H====
    P3CR = 0x08;
    P3 = 0x08;
    // 配置Uart工作在模式1
    select_bank1();
    // 0110 0111 Tx:P3.3   Rx：P3.4
    UART0CR = 0x67;
    select_bank0();
    SCON = 0x50;
    /*配置波特率参数，波特率9600*/
    /* 计算公式：(int)X=FSY/(16*波特率) ;  SBRT=32768-X  ;   SFINE=（FSY/波特率）-16*X   FSY=8M*/
    // 波特率发生器高位
    SBRTH = 0xFF;
    // 波特率发生器低位
    SBRTL = 0x64;
    // 波特率发生器微调
    SFINE = 0x04;
    // 使能串口中断
    // IEN0 |= 0x10;
}

/**
 * @brief 发送缓冲区数据
 */
static void Uart0_Transmit(void) {
    ptr_tx0 = &gUart0DataTxD[0];
    SBUF = *ptr_tx0;
    if (gUart0Len_TxD > 0)
        gUart0Len_TxD--;
    ptr_tx0++;
}

/**
* @brief 发送一个字节
*/
void Uart0_Send_Byte(U8 byte) {
    Uart0_Send_Bytes(&byte, 1);
}

/**
 * @brief 发送数组
 * @param bytes
 * @param len
 */
void Uart0_Send_Bytes(const U8 *bytes, U8 len) {
    memcpy(gUart0DataTxD, bytes, len);
    gUart0Len_TxD = len;
    Uart0_Transmit();
}

/**
 * @brief 发送字符串
 * @param str
 * @param len
 */
void Uart0_Send_String(const char *str) {
    U8 len = strlen(str);
    memcpy(gUart0DataTxD, str, len);
    gUart0Len_TxD = len;
    Uart0_Transmit();
}

/**
* @brief printf 重定向
* @param c
*/
void putchar(char c){
    SBUF = c;
    while(!TI);
    TI = 0;
}

/**
* @brief UART0中断
**/
void INT_EUART0(void) interrupt 4{
    if(TI){
        TI = 0;
        if(gUart0Len_TxD >0){
					SBUF = *ptr_tx0;
					gUart0Len_TxD --;
					ptr_tx0 ++;
        }
    }
}