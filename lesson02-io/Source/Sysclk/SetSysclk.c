#include "SH79F9476.h"
#include "intrins.h"
#include "api_ext.h"
#include "cpu.h"
#include "sysclk_define.h"
/*****************************************************************************************************
 *  Function Name: void SetSystemClk()
 *  Created By:    xiong.zhang
 *  Created Date:  2021-05-31
 *  Input:         None
 *  Output:        None
 *  Description:   设置系统时钟 
 *  Modified By:   
 *  Modified Date:
 *  History:
 *  Others:                        
 *****************************************************************************************************/
void SetClk()
 {
#ifdef 	HIGH_FREQUENCE 
	  CLKCON =0x08;
	  Delay();
	  CLKCON|=0x04;
#endif

#ifdef 	LOW_FREQUENCE  
	  CLKCON &=0xFB;	  //设置FS为0，选择OSC1CLK为系统时钟
	  _nop_();
	  CLKCON &=0xF7;      //关闭高频时钟OSC2CLK
	  _nop_();
	  _nop_();
	  _nop_();
	  _nop_();
	  CLKCON &=0x9F;	  //关闭分频  Fsys=Fosc
#endif
 }
