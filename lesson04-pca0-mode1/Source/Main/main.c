#include "SH79F9476.h"
#include "cpu.h"
#include "intrins.h"
#include "api_ext.h"
#include "irq_util.h"
#include "clk_util.h"
#include "pca0_util.h"

void main()
{
	// 开启中断
	enAllIrq();
	enablePca0Irq();
	
	// P1.0 P1.1 设置为输出模式
	P1CR |= 0x03;
	P1 = 0x00;
    // 选择高速时钟
    highFrequenceClk();
    PCA0Mode1();
    while (1);
}
