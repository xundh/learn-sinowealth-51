#include "SH79F9476.h"
#include "cpu.h"
#include "intrins.h"
#include "api_ext.h"
#include "clk_util.h"
#include "euart_utils.h"
#include "irq_util.h"
#include "common_utils.h"


void main()
{
	// 选择高速时钟
	highFrequenceClk();
	
	// 串口初始化
	Uart0_Init();
	// 开启中断
	enableAllIrq();
	Uart0_Send_Byte(0x32);
	while (1){
        Uart0_Send_String("Hello");
        delay_ms(500);
	}
}
