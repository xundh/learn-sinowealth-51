//
// Created by hs26661 on 2024/4/19.
//

#ifndef LESSON07_ADC_ONE_COMMON_UTILS_H
#define LESSON07_ADC_ONE_COMMON_UTILS_H
#include "cpu.h"

void delay_us(void);

void delay_ms(U16 ms);

#endif //LESSON07_ADC_ONE_COMMON_UTILS_H
