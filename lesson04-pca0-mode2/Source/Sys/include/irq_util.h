#ifndef __IRQ_UTIL_H__
#define __IRQ_UTIL_H__
#include "SH79F9476.h"
/**
* @brief: 开启所有中断
*/
void enAllIrq(void);

/**
* @brief: 开启PCA0中断
*/
void enablePca0Irq(void);

#endif
