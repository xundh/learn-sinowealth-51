#ifndef __PCA0_UTIL_H__
#define __PCA0_UTIL_H__

#include "SH79F9476.h"
#include "api_ext.h"
#include "intrins.h"
/**
* @brief PCA0 工作方式0 边沿触发
*/
void PCA0Mode0(void);
#endif
