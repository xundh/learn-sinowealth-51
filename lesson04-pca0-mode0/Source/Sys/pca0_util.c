#include "pca0_util.h"

/**
* @brief PCA0 工作方式0 边沿触发
*/
void PCA0Mode0(void){
    select_bank1();
    // 工作在单沿模式、禁止PCA0溢出中断、选择系统时钟作为PCA0时钟源
  P0CMD=0x00;
    // 捕捉方式、任意沿触发、打开捕捉/比较中断、使能比较/捕捉模块0
  P0CPM0=0x39;
    // 配置计数器最大值
  P0TOPH=0x08;
  P0TOPL=0x00;
    // 启动PCA0计数器
  PCACON=0x1;
    select_bank0();
}
/**
* @brief PCA0中断
*/
void INT_PCA0(void) interrupt 20
{ 
  _push_(INSCON);
  Select_Bank1();
	// 清除溢出标志
  P0CF = 0x00;
	P0CPL0 = P0CPL0+1;
	Select_Bank0();
	P1_0=~P1_0;
  _pop_(INSCON);       
}
