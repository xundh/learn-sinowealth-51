#include "SH79F9476.h"
#include "cpu.h"
#include "intrins.h"
#include "api_ext.h"

void main()
{
    // 时钟设置高速模式
    CLKCON = 0x08;
    Delay();
    CLKCON |= 0x04;

    // P0.0,P0.1设置为输出
    P0CR = 0x03;
    P0 = 0x00;

    // 允许所有中断
    IEN0 |= 0x80;
    // 打开定时器2中断 
    IEN1 |= 0x04;
    // 设置定时器工作在重载模式，忽略T2EX引脚检测
    T2CON = 0;    
    // 选择定时器为递增定时器，时钟源为系统时钟
    T2MOD = 0x80; 
    // 定时12000*1/24=500us
    TL2 = 0x1F;   
    TH2 = 0xD1;
    // 重载计数器 Value
    RCAP2L = 0x1F; 
    RCAP2H = 0xD1;
    // 启动定时器
    T2CON |= 0x04; 

    while (1)
        ;
}
// TIMER2的中断
void INT_TIMER2(void) interrupt 9
{
    _push_(INSCON);
    Select_Bank0();

    // 定时器溢出
    if (T2CON & 0x80)
    {
        // 溢出标志位清0
        T2CON &= 0x7F;
        // 翻转P0_0
        P0_0 = ~P0_0;
    }
    // 检测到外部事件下降沿
    if (T2CON & 0x40)
    {
        // 1011 1111， T2EX引脚外部事件被检测到的标志位清0
        T2CON &= 0xBF;
        // 翻转P0_1
        P0_1 = ~P0_1;
    }
    _pop_(INSCON);
}
