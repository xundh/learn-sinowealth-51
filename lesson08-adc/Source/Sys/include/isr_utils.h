#ifndef __IRQ_UTIL_H__
#define __IRQ_UTIL_H__
#include "SH79F9476.h"
/**
* @brief: 开启所有中断
*/
void enableAllIsr(void);

/**
* @brief: 开启PCA0中断
*/
void enablePca0Isr(void);

/**
 * @brief 开启uart0中断
*/
void enableEUart0Isr(void);
/**
* @brief 开启ADC中断
*/
void enableAdcIsr(void);
#endif
