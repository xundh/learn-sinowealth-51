#include "intrins.h"
#include "euart_utils.h"
#include "api_ext.h"
#include "SH79F9476.h"
#include "cpu.h"
/**
* @brief 初始化串口
*/
void uart0_init(){
    //=====TX 建议配置为输出H====
    P3CR = 0x08;
    P3 = 0x08;
    // 配置Uart工作在模式1
    select_bank1();
		// 0110 0111 Tx:P3.3   Rx：P3.4
    UART0CR = 0x67;
    select_bank0();
    SCON=0x50; 
    /*配置波特率参数，波特率9600*/
    /* 计算公式：(int)X=FSY/(16*波特率) ;  SBRT=32768-X  ;   SFINE=（FSY/波特率）-16*X   FSY=8M*/
		// 波特率发生器高位
    SBRTH = 0xFF;
		// 波特率发生器低位
    SBRTL = 0x64;
		// 波特率发生器微调
    SFINE = 0x04;
}
/**
* @brief 发送一个字节
*/
void uart0_send_byte(u8 byte){
    SBUF = byte;
    while(!TI);
    TI = 0;
}
/**
* @brief UART0中断
**/
void INT_EUART0(void) interrupt 4{
	u8 dat;
	_push_(INSCON);
	
	// 收的数值+1 再返回去
	while(!RI);
	dat = SBUF;
	RI = 0;   
	_pop_(INSCON); 

	dat ++;
	uart0_send_byte(dat);      
}
