#include "SH79F9476.h"
#include "clk_utils.h"
#include "cpu.h"
#include "isr_utils.h"
#include "adc_utils.h"
#include "euart_utils.h"

void delay_us(void) {
    UINT16 i;
    for (i = 0; i < 1000; i++);
}

void delay_ms(UINT16 ms) {
    UINT16 i;
    for (i = 0; i < ms; i++)
        delay_us();
}

void main() {
    UINT16 result;
    // 选择高速时钟
    highFrequenceClk();

    // 初始化串口
    uart0_init();

    // 开启中断
    enableAllIsr();
    enableAdcIsr();
    init_adc_one();

    while (1) {
        // ADC转换
        result = adc_one_trans();
        // 高位
        uart0_send_byte((u8)(result >> 8));
        // 低位
        uart0_send_byte((u8) result);
        // 暂停1s
        delay_ms(600);
    }
}
