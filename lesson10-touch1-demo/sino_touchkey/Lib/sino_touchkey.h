#ifndef __TOUCHKEY_H
#define	__TOUCHKEY_H

#include "../Inc/c51_type.h"
#include "../Inc/SH79F9476.H"
#include "../Lib/tk_conf.h"
#include "../Inc/tk_conf_add.h"
/*=============寄存器相关配置======================================================*/
#define  SETBANK0             INSCON &= 0xBF;
#define  SETBANK1             INSCON |= 0x40;
#define  RSET_WATCHDOG        RSTSTAT &= 0x78;
#define  SET_MCU_CLK_RC_24M   CLKCON = (CLKCON & 0x9F);
#define  SET_MCU_CLK_RC_12M   CLKCON = (CLKCON & 0x9F) | 0x20;

#define  Enable_Touch         TKCON1 |= 0x80;
#define  Disable_Touch        TKCON1 &= 0x7F;
#define  TK_GO                TKCON1 |= 0x40;
#define  TK_Scan_is_End      (TKF0 & 0x01) != 0x00
#define  CLR_ALL_TK_IF        TKF0 = 0x00;
#define  TK_SAMP_SEL_18CNT    TKCON1 = (TKCON1 & 0xF8) | 0x04;
#define  TK_SAMP_SEL_34CNT    TKCON1 = (TKCON1 & 0xF8) | 0x05;
#define  TK_SAMP_SEL_66CNT    TKCON1 = (TKCON1 & 0xF8) | 0x06;

#define  TK_CADJ_RAM_ADDR          0x1000
#define  TK_DATA_RAM_ADDR          0x102A
/*============================变量参数配置=====================================================*/
#define Filter_Index        2
#define Filter_Deg          4		
#define Filter_Frequency    13
#define SPEAK_VOLUME	    0x88
#define TKST_IS_High_Fre	1
#define TKST_IS_Low_Fre		0
#define TK_Adjust_OK		1
#define	TK_Adjust_Wrong		0 
/*=============================================================================================*/
#define	BIT0	            0x01
#define	BIT1	            0x02
#define	BIT2	            0x04
#define	BIT3	            0x08
#define	BIT4	            0x10
#define	BIT5	            0x20
#define	BIT6	            0x40
#define	BIT7	            0x80
/*===========================手动校准开关为0，为了兼容uart.c不做改动=============================*/
#define L_H_delta			300
#define L_Pre_WT            30  	//
#define L_Cnt_WT            2   	//
#define L_Pre_FT            70  	//
#define L_wt_Speed 			3   	//

#define L_BaseCnt_set   	20
#define L_downCnt           4	    //
#define key_fastcounter_set 8  		//

#define L_H_delta_max	    (L_H_delta+ L_H_delta*30/100)

#define L_CSI_cnt_set	    50		
#define  L_Frequcecy_div    6       //
#define  L_Frequcecy_filter 6       //
#define  L_key_WT_set       0x13    //
#define  L_WT_Delay			50
#define  L_Inter1  			13        
#define  data_add           3     	//
/*=========================校准相关配置参数==================================================*/
#define	TK_GO_ERR			0x01
#define	TK_Count_Over		0x02
#define	TK_Data_Over		0x03
#define	TK_Going			0x05
#define	TK_Scan_End			0x06
#define TK_BL_Collecting	0x07
#define	TK_Diff_Collecting	0x08
#define	TK_Adjust_Success	0x09
#define	TK_Data_Over1		0x0A
#define	TK_Adjust_Fail			0x0B
#define	TK_New_BL_Collecting	0x0C
#define	TK_New_Diff_Collecting	0x0D
//===============校准卡测上下限值=============================================================
#define   upper_limit   520//
#define   lower_limit   506//
#define   max_limit_low	  12
#define	  max_limit_up    100
#define   baseline_limit  800
/*==================参数声明==================================================================*/
/*============================================================================================*/
/*=========new frequency jump=================================================================*/
extern U8  xdata  Fre_num_L;
extern U16 xdata  key_filter_L[KEY_NUMBER1][Filter_Frequency];
extern U8  xdata  Frequcecy_div_set;
extern U8  xdata  Frequcecy_filter_set;
extern U16 xdata  tempdata_L[Filter_Frequency];
extern U8  xdata  key_fastcounter;
extern U8  xdata  Fre_switch_Cnt;
extern U8  xdata  Fre_switch_Set;
extern U8  xdata  R_TKstep_Fre_Lmsg;
extern U8  xdata  R_TKstep_Fre_Hmsg;
extern U8  xdata  R_TKstep_Fre_H_step3_6;
extern U8  xdata  R_Fre_ST;
/*==========开短路===========================================================================*/
extern U32 xdata  sns_short_flag;				//短路标志
extern U32 xdata  sns_open_flag;		 		//开路标志
extern U32 xdata  sns_Pseudo_Soldering_flag; 	//虚焊等其他异常
/*==============特殊功能=====================================================================*/
extern U8  xdata  Add_Set;
extern U8  xdata  Add_function; 
/*===========================================================================================*/
extern U8  xdata  LF_key_WtCnt;
extern U8  xdata  key_WT_set;
extern U8  xdata  FLAG_COUNT_set;
extern U8  xdata  init_BaseCnt_set;
extern U16 xdata  LF_key_RAM[KEY_NUMBER1];
extern U16 xdata  LF_key_RAM_pre[KEY_NUMBER1];
extern U8  xdata  LF_nake_stblecount[KEY_NUMBER1];
extern U8  xdata  nake_Flag[KEY_NUMBER1];
extern U16 xdata  nake_limit_value_set;
extern U8  code   Tab_Chn[KEY_NUMBER1];
extern U8  xdata  key_sample_count;
extern U8  xdata  tk_num;
extern U8  xdata  R_downCnt;
extern U8  xdata  datasub_counter;
extern U8  xdata  dataplus_counter;
extern U8  xdata   multikey_control;
extern U8  code   key_threshold_per[KEY_NUMBER1];
extern U16 xdata  R_Reset_Cnt;
extern U16 xdata  R_Reset_Cnt_H;
extern U16 xdata  R_HF_MaxLimit;
extern U8  xdata  R_data_add;
/*=======校准参数================================================================================*/
extern U16  xdata  upper_limit_set;
extern U16  xdata  lower_limit_set;
extern U8   xdata  k_temp;
extern bit	TK_Adjust_Caj_flag;
extern bit	TK_Adjust_Caj_err;   
/*================================================================================================*/
extern U32 xdata sino_key_value;
extern U32 xdata sino_key_value_pre;
extern U8  xdata line;  //分组变量	 	 
extern U16 xdata  HF_key_step_max[KEY_NUMBER1];
extern U16 xdata  HF_key_baseline[KEY_NUMBER1];
extern U16 code   HF_key_threshold[KEY_NUMBER1];
extern U16 xdata  HF_key[KEY_NUMBER1];
extern U16 xdata  HF_DIV[KEY_NUMBER1];
#if Touch_Adjust==1
extern U16 xdata  HF_key_max_diff[KEY_NUMBER1];
#endif
extern U16 xdata  HF_key_filter[KEY_NUMBER1][Filter_Deg];
extern U8  xdata  HF_key_sub_val[KEY_NUMBER1];
extern U8  xdata  HF_keytouch_flag[KEY_NUMBER1];
extern U8  xdata  HF_key_flag[KEY_NUMBER1];
extern U8  xdata  HF_baseline_sub_flag[KEY_NUMBER1];
extern U8  xdata  HF_baseline_plus_flag[KEY_NUMBER1];
extern U8  xdata  HF_key_count_add[KEY_NUMBER1];
extern U8  xdata  HF_key_count_sub[KEY_NUMBER1];

extern U8  xdata  datasub_counter;
extern U8  xdata  dataplus_counter;
extern U8  data   HF_CSI_cnt;

extern U16 code   LF_key_max[KEY_NUMBER1];
extern U16 code   HF_key_max[KEY_NUMBER1];
extern U8  code   key_div_per[KEY_NUMBER1] ;

extern U16 xdata LF_key_step_max[KEY_NUMBER1];
extern U16 xdata LF_key_baseline[KEY_NUMBER1];

extern U16 code LF_key_threshold[KEY_NUMBER1];
extern U16 xdata LF_key[KEY_NUMBER1];
extern U16 xdata LF_DIV[KEY_NUMBER1];
#if Touch_Adjust==1
extern U16 xdata LF_key_max_diff[KEY_NUMBER1];
#endif
extern U16 xdata LF_key_filter[KEY_NUMBER1][Filter_Deg];
extern U8  xdata LF_key_sub_val[KEY_NUMBER1];
extern U8  xdata LF_keytouch_flag[KEY_NUMBER1];
extern U8  xdata LF_key_flag[KEY_NUMBER1];
extern U8  xdata LF_baseline_sub_flag[KEY_NUMBER1];
extern U8  xdata LF_baseline_plus_flag[KEY_NUMBER1];
extern U8 	xdata LF_key_count_add[KEY_NUMBER1];
extern U8 	xdata LF_key_count_sub[KEY_NUMBER1];
extern U8  data  LF_CSI_cnt;
extern U16 code  *key_max;
extern U16 xdata *key_step_max;
extern U16 xdata *key_baseline;
extern U16 code *key_threshold;
extern U16 xdata *key;
#if Touch_Adjust==1
extern U16 xdata *key_max_diff;
#endif
extern U16 xdata *key_filter[KEY_NUMBER1];
extern U8  xdata *key_sub_val;
extern U8  xdata *keytouch_flag;
extern U8  xdata *key_flag;
extern U8  xdata *baseline_sub_flag;
extern U8  xdata *baseline_plus_flag;
extern U8  xdata *key_count_add;
extern U8  xdata *key_count_sub;
extern U8  xdata  CSI_cnt;
extern U8 	xdata CSI_cnt_set;
extern U16 xdata  Max_delta;
extern U16 xdata  LF_key_threshold2;
extern U16 xdata  LF_key_threshold3;
extern U8  xdata  delta_Set;
extern U8  xdata  R_Cnt_WT;

#if ((Function&0x01) ==0x01)
extern xdata U8 R_water_add_count ;
extern xdata U8 R_water_Delay ;
extern xdata U8 R_water_num ;
extern xdata U8 R_Inter1 ;
#endif
/*================================================================================================*/
extern U8  xdata *pb;
extern U8  xdata *pa;
extern bit tk_init_req;
extern bit tk_adjust_flag,fre_switch_flag;
extern U16 xdata *DIV;
extern U8  xdata *R_DATA_RAM_ADDR;
extern U8  xdata *C_CADJ_RAM_ADDR;
extern bit TK_going_flag;
extern bit TK_Scan_End_flag;
extern bit	 tkst_flag,tkst_flag_pre;
extern bit  wt_dw;
/*=============================Timer变量==========================================================*/
extern bit f_5ms,f_30ms;
void timerinit();
/*========================================函数声明===============================================*/
extern void touchkeyinit();
extern void Get_TK_States();
extern void TK_start();
extern void Touchkey_adjust_cadj_inter();
void touchkey_go();
void touchkey_ad_go();	
void EnTk(U8 n);
void EnIO(U8 n);
void tk_para_init();
void DisTk(void);
void GetTouchkeyData();
void key_judge();
void get_max_diff();
void Rst_WDT();
void Touchkey_adjust();
void Touchkey_adjust_cadj();
void touchkey_scan();
void tk_para_auto_adjust();
void touchkey_rschk();	
void TK_WaterProof_test();
void tk_samp_fre_set();
void tk_fre_update();
void touchkey_samp_fre_set();
void TK_HF_initSet();
void touchkey_reset_chk();
void get_cs_data();
#if Check_KeyChl ==1
void sns_self_check();
#endif
/*================================================================================================*/
#endif
