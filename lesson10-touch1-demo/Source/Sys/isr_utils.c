#include "isr_utils.h"

/**
* @brief: 开启所有中断
*/
void enableAllIsr(void)
{
  IEN0 |= 0x80;
	EA=1;
}
/**
* @brief: 开启PCA0中断
*/
void enablePca0Isr(void)
{
    IEN2 |= 0x20;
}
/**
 * @brief 开启uart0中断
*/
void enableEUart0Isr(void){
    IEN0 |= 0x10;
}
/**
* @brief 开启ADC中断
*/
void enableAdcIsr(void){
    IEN0 |= 0x40;
}
