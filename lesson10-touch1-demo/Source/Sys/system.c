#include "SH79F9476.h"
#include "intrins.h"
#include "c51_type.h"
void Delay()
{
	U8 i=0x0a;
	while(i--);
}
 /*****************************************************************************************************
 *  Function Name: select_bank0
 *  Created By:    xiong.zhang
 *  Created Date:  2021-05-31
 *  Input:         None
 *  Output:        None
 *  Description:   切换到寄存器bank0 
 *  Modified By:   
 *  Modified Date:
 *  History:
 *  Others:                        
 *****************************************************************************************************/
void select_bank0()
{
      INSCON=0x00;
}
 /*****************************************************************************************************
 *  Function Name: select_bank1
 *  Created By:    xiong.zhang
 *  Created Date:  2021-05-31
 *  Input:         None
 *  Output:        None
 *  Description:   切换到寄存器bank0 
 *  Modified By:   
 *  Modified Date:
 *  History:
 *  Others:                        
 *****************************************************************************************************/
void Select_bank1()
{
     INSCON=0x40;
}
 /*****************************************************************************************************
 *  Function Name: power_pd
 *  Created By:    xiong.zhang
 *  Created Date:  2021-05-31
 *  Input:         None
 *  Output:        None
 *  Description:   系统进入PD模式 
 *  Modified By:   
 *  Modified Date:
 *  History:
 *  Others:                        
 *****************************************************************************************************/
void power_pd()
{
	 SUSLO=0x55;
	 PCON=0x02;
	 _nop_();
	 _nop_();
	 _nop_();
}
 /*****************************************************************************************************
 *  Function Name: power_idle
 *  Created By:    xiong.zhang
 *  Created Date:  2021-05-31
 *  Input:         None
 *  Output:        None
 *  Description:   系统进入IDLE模式 
 *  Modified By:   
 *  Modified Date:
 *  History:
 *  Others:                        
 *****************************************************************************************************/
 void power_idle()
{
	 SUSLO=0x55;
	 PCON=0x01;
	 _nop_();
	 _nop_();
	 _nop_();
}
