#ifndef __UEART_UTILS_H__
#define __UEART_UTILS_H__

#include "c51_type.h"

#define UART0_DATA_BUF_SIZE 244

/**
* @brief 初始化串口
*/
void Uart0_Init();

/**
 * @brief 发送缓冲区数据
 */
void Uart0_Transmit(U8 len);
/**
 * @brief 向gUart0DataTxD尾部添加数组,注意要考虑到如果添加的过长，就回到队列头部添加剩余部分
 */
void Uart0_Append_Bytes(const char *bytes, U8 len);
#endif
