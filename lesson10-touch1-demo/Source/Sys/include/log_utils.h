#ifndef __LOG_UTILS_H__
#define __LOG_UTILS_H__
#include "euart_utils.h"
#include <stdio.h>

// 发送缓冲区
extern U8 gUart0DataTxD[UART0_DATA_BUF_SIZE];
#define TAG log_str+log_len

// 日志等级
#define LOG_LEVEL_NONE 0
#define LOG_LEVEL_ERROR 1
#define LOG_LEVEL_INFO 2

// 判断预定义的宏 LOG_LEVEL

#if LOG_LEVEL >= LOG_LEVEL_INFO
/**
 * INFO 级别日志
 */
#define LOGI(args) \
    do {           \
        U8 log_len; \
        U8 log_str[200];           \
        log_len = sprintf(log_str, "[I] %s:%bd: ", __FILE__, __LINE__); \
        log_len += sprintf args;           \
        log_len += sprintf(log_str + log_len, "\n"); \
        Uart0_Append_Bytes(log_str, log_len); \
    } while (0)
#else
#define LOGI(args) (void)0
#endif

#if LOG_LEVEL >= LOG_LEVEL_ERROR
/**
 * ERROR 级别日志
 */
#define LOGE(args) \
    do {           \
        U8 log_len;\
        U8 log_str[200];           \
        log_len = sprintf(log_str, "[E] %s:%bd: ", __FILE__, __LINE__); \
        log_len += sprintf args;           \
        log_len += sprintf(log_str + log_len, "\n"); \
        Uart0_Append_Bytes(log_str, log_len); \
    } while (0)
#else
#define LOGE(args) (void)0
#endif

#endif