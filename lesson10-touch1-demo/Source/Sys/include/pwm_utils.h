#ifndef __PWM_UTILS_H__
#define __PWM_UTILS_H__

#include "SH79F9476.h"
#include "intrins.h"
#include "cpu.h"
#include "api_ext.h"
/**
 * @param frequency 频率
 * @param duty 占空比,单位是百分比
 * @param polar 极性
 * @brief 初始化PWM0
 */
void Pwm0_Init(volatile U16 frequency, volatile U16 duty, volatile U16 polar);

/**
 * @param frequency 频率
 * @param duty 占空比,单位是百分比
 * @param polar 极性
 * @brief 初始化PWM1
 */
void Pwm1_Init(volatile U16 frequency, volatile U16 duty, volatile U16 polar);
#endif
