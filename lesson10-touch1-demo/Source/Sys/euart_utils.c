#include "intrins.h"
#include "euart_utils.h"
#include "api_ext.h"
#include "SH79F9476.h"
#include "c51_type.h"
#include <stdio.h>
#include "string.h"

// 发送缓冲区
static U8 gUart0DataTxD[UART0_DATA_BUF_SIZE];
// 未发送数据长度
static U8 gUart0DataTxDLen;

// 内部变量，发送指针
static volatile U8 *ptr_tx0_head;

/**
* @brief 初始化串口
*/
void Uart0_Init() {
    //=====TX 建议配置为输出H====
    P3CR = 0x08;
    P3 = 0x08;
    // 配置Uart工作在模式1
    select_bank1();
    // 0110 0111 Tx:P3.3   Rx：P3.4
    UART0CR = 0x67;
    select_bank0();
    SCON = 0x50;
    /*配置波特率参数，波特率 9600*/
    /* 计算公式：(int)X=FSY/(16*波特率) ;  SBRT=32768-X  ;   SFINE=（FSY/波特率）-16*X   FSY=8M*/
    // 波特率发生器高位
    SBRTH = 0xFF;
    // 波特率发生器低位
    SBRTL = 0x64;
    // 波特率发生器微调
    SFINE = 0x04;
    // 使能串口中断
    IEN0 |= 0x10;
	
		ptr_tx0_head = &gUart0DataTxD[0];
}

/**
 * @brief 发送缓冲区数据
 */
void Uart0_Transmit(U8 len) {
		
			gUart0DataTxDLen = len;
			SBUF = *ptr_tx0_head;
			if (gUart0DataTxDLen > 0)
					gUart0DataTxDLen--;
			if (ptr_tx0_head >= &gUart0DataTxD[UART0_DATA_BUF_SIZE]) {
					ptr_tx0_head = &gUart0DataTxD[0];
			} else {
					ptr_tx0_head++;
			}
		
}
/**
 * @brief 向gUart0DataTxD尾部添加数组,注意要考虑到如果添加的过长，就回到队列头部添加剩余部分
 */
void Uart0_Append_Bytes(const char *bytes, U8 len) {
    U8 i  ,startIndex;
    // 如果添加的长度超过缓冲区长度，就只添加缓冲区长度的数据
    if (len > UART0_DATA_BUF_SIZE) {
        len = UART0_DATA_BUF_SIZE;
    }
    startIndex = ptr_tx0_head - &gUart0DataTxD[0] + gUart0DataTxDLen;
    for (i = 0; i < len; i++) {
        gUart0DataTxD[startIndex] = bytes[i];
        startIndex++;
        // 如果添加的数据长度超过了缓冲区长度，就回到队列头部添加剩余部分
        if (startIndex == UART0_DATA_BUF_SIZE) {
            startIndex = 0;
        }
    }
    Uart0_Transmit(gUart0DataTxDLen+len);
}
/**
* @brief UART0中断
**/
void INT_EUART0(void) interrupt 4{
    if(TI){
        TI = 0;
        if(gUart0DataTxDLen >0){
            SBUF = *ptr_tx0_head;
            gUart0DataTxDLen --;
            // 这里产生了一种情况，如果发送的数据长度超过了缓冲区长度，就会导致ptr_tx0指针超出范围
            if(ptr_tx0_head >= &gUart0DataTxD[UART0_DATA_BUF_SIZE]){
                ptr_tx0_head = &gUart0DataTxD[0];
            }else{
                ptr_tx0_head ++;
            }
        }
    }
}