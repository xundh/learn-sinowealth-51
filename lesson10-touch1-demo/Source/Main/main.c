#include "isr_utils.h"
#include "../sino_touchkey/Lib/sino_touchkey.h"
#include "log_utils.h"


void main() {
	
	// 选择高速时钟
	SET_MCU_CLK_RC_24M
	
	enableAllIsr();
	
	// 初始化串口
	Uart0_Init();
	LOGI((TAG,"start s=%bd", 0));
	
	timerinit();
	Touchkey_adjust();
	touchkeyinit();       //touch key 初始化
	while(1)
	{        		
			RSTSTAT = 0;
			if(f_5ms)
			{
					f_5ms = 0;
					Get_TK_States();           // 按键扫描     
					if(sino_key_value_pre != sino_key_value){
						sino_key_value_pre = sino_key_value;
						LOGI((TAG,"touch key=%ld", sino_key_value));
				}
	
			}		   	
	}
}
