#include "SH79F9476.h"
#include "cpu.h"
#include "intrins.h"
#include "api_ext.h"
#include "clk_util.h"
#include "euart_utils.h"
#include "irq_util.h"


void main()
{
	// 选择高速时钟
	highFrequenceClk();
	
	// 串口初始化
	uart0_init();
	// 开启中断
	enableAllIrq();
	enableEUart0();
	uart0_send_byte(0x32);
	while (1);
}
