#ifndef __UEART_UTILS_H__
#define __UEART_UTILS_H__

#include "cpu.h"

/**
* @brief 初始化串口
*/
void uart0_init();

/**
* @brief 发送一个字节
*/
void uart0_send_byte(u8 byte);
#endif
