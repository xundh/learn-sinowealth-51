#ifndef __IRQ_UTIL_H__
#define __IRQ_UTIL_H__
#include "SH79F9476.h"
/**
* @brief: 开启所有中断
*/
void enableAllIrq(void);

/**
* @brief: 开启PCA0中断
*/
void enablePca0Irq(void);

/**
 * @brief 开启uart0中断
*/
void enableEUart0(void);
#endif
