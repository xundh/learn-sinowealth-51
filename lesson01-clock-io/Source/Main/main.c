
#include "SH79F9476.h"
#include "api_ext.h"
#include "cpu.h"
#include "sysclk_define.h"

void Sysclk_Test()
{
  P1CR |=0x01;    //配置Port P1端口的bit0为输出模式 
  P1_0 = 0;       //控制P1_0pin输出低电平
  #ifdef Half_VDD
    P0V0=0x0f;
  #endif
  SetClk(); 
  while(1)	      //控制P0_0pin翻转测试系统clk
  {
    P1_0=~P1_0;
  }
}

void main()
{
  Sysclk_Test();
	while(1);
}