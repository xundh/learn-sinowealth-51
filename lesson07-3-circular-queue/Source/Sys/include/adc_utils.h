#ifndef __ADC_UTILS_H__
#define __ADC_UTILS_H__

#include "SH79F9476.h"
#include "intrins.h"
#include "cpu.h"
#include "api_ext.h"

/**
 * @brief ADC的单一通道转换（AN0）。
 *
 * 配置ADC使用VDD作为电压参考，没有触发。
 * 系统时钟设置为24M，ADC设置为50ksps采样。
 * 软件开始ADC转换，结果存储在ADC_res数组中。
 */
void init_adc_one(void);
UINT16 adc_one_trans(void);

/**
 * @brief ADC的多通道转换（AN0~AN7）。
 */
void init_adc_array(void);
void adc_array_trans(void);

#endif