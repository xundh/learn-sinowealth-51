#ifndef __PCA0_UTIL_H__
#define __PCA0_UTIL_H__

#include "SH79F9476.h"
#include "api_ext.h"
#include "intrins.h"
/**
* @brief PCA0 工作方式0 边沿触发
*/
void PCA0Mode0(void);

/**
* @brief PCA0 工作方式1 软件定时器
*/
void PCA0Mode1(void);

/**
* @brief PCA0 工作方式2 频率输出方式
*/
void PCA0Mode2(void);

/**
* @brief PCA0 工作方式3 PWM8模式
*/
void PCA0Mode3PWM8(void);

/**
* @brief PCA0 工作方式3 PWM16模式
*/
void PCA0Mode3PWM16(void);
#endif
