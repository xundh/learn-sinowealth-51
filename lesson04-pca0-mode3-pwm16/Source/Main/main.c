#include "SH79F9476.h"
#include "cpu.h"
#include "intrins.h"
#include "api_ext.h"
#include "irq_util.h"
#include "clk_util.h"
#include "pca0_util.h"

void main()
{
	// 开启中断
	enAllIrq();
	enablePca0Irq();
	
    // 选择高速时钟
    highFrequenceClk();
    PCA0Mode3PWM16();
    while (1);
}
