# 中颖51单片机学习

博客地址：https://blog.csdn.net/xundh/category_12110844.html

- lesson01: 时钟和IO操作
- lesson02: IO操作
- lesson03: 定时器2的3种工作方式
- lesson04-mode0: PCA0工作方式0 边沿触发的捕捉模式
- lesson04-mode1: PCA0工作方式1 软件定时器模式
- lesson04-mode2: PCA0工作方式2 频率输出方式
- lesson04-mode3-pwm8: PCA0工作方式3 PWM8模式
- lesson04-mode3-pwm16: PCA0工作方式3 PWM16模式
- lesson04-mode3-xpwm16: PCA0工作方式3 相位修正XPWM16模式
- lesson04-mode3-xppwm16: PCA0工作方式3 相频修正XPPWM16模式
- lesson05-eeprom-random-id: 读取出厂预置的随机 ID
- lesson05-eeprom-op：类EEPROM 操作示例
- lesson06-euart: 串口通信 工作方式1
- lesson06-euart-sendbuf：通过中断发送串口数据
- lesson07-printf： printf重定向到串口与自定义日志输出函数
- lesson08-adc: ADC模数转换
- lesson09-pwm0: PWM（12bit脉冲宽度调制）
- lesson10-touch0-new_project: 新建触摸按键工程
- lesson10-touch1-demo: 触摸按键功能集成