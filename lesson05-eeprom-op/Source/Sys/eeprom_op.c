#include "eeprom_op.h"
#include "SH79F9476.h"
#include "intrins.h"
#include "ABSACC.H"

UCHAR ssp_flag;

/**
* @brief 扇区擦除
**/
void EEPromSectorErase(UCHAR nAddrH)
{
	// 保护中断			
     _push_(IEN0);
    //关总中断	 
     IEN0 &=0x7F;
    // 访问EEPROM区
	FLASHCON = 0x01;									
	// 扇区在bit3-bit1
	XPAGE = nAddrH<<1 ;
	// E6 扇区擦除
	IB_CON1	 = 0xE6;
	// 编程时IB_CON2必须为05H
	IB_CON2	 = 0x05;
	// 编程时IB_CON3必须为0AH
	IB_CON3	 = 0x0A;
	// 编程时IB_CON4必须为09H
	IB_CON4	 = 0x09;
	// 增加flag判断，加强抗干扰能力
	if(ssp_flag!=0xA5)
		goto Error;
	// 编程时IB_CON5必须为06H
	IB_CON5	 = 0x06;
	_nop_();				
	_nop_();
	_nop_();
	_nop_();	
Error:
	ssp_flag = 0;
	IB_CON1 = 0x00;
	IB_CON2 = 0x00;
	IB_CON3 = 0x00;
	IB_CON4 = 0x00;
	IB_CON5 = 0x00;
	// 切回Flash区
	FLASHCON = 0x00;
	// 操作结束，恢复总中断
    _pop_(IEN0);
}

/**
* @brief 字节读
**/
UCHAR EEPromByteRead(UCHAR nAddrH,UCHAR nAddrL){
	UCHAR nTemp;
	// 保护现场
     _push_(IEN0);
    // 关总中断	 
     IEN0 &=0x7F;
	// 访问 EEPROM 区
	FLASHCON = 0x01;
    // 读取相应地址数据
	nTemp= CBYTE[nAddrH*256+nAddrL];
    // 切回 Flash 区
	FLASHCON = 0x00;
	// 操作结束，恢复现场
    _pop_(IEN0);
	return (nTemp);
}

/**
* @brief 字节编程
**/
void EEPromByteProgram(UCHAR nAddrH,UCHAR nAddrL, UCHAR nData){
	// 保护现场
    _push_(IEN0);
    // 关总中断
    IEN0 &=0x7F;
    // 访问 EEPROM 区
	FLASHCON = 0x01;	
	// 从0x00开始
	XPAGE = nAddrH;
	IB_OFFSET = nAddrL;
	// 烧写内容
	IB_DATA = nData;
	IB_CON1	 = 0x6E;
	// 编程时IB_CON2必须为05H
	IB_CON2	 = 0x05;
	// 编程时IB_CON3必须为0AH
	IB_CON3	 = 0x0A;
	// 编程时IB_CON4必须为09H
	IB_CON4	 = 0x09;
    // 增加flag判断，加强抗干扰能力
	if(ssp_flag!=0x5A)
		goto Error;
	// 编程时IB_CON5必须为06H
	IB_CON5	 = 0x06;
	_nop_() ;
	_nop_() ;
	_nop_() ;
	_nop_() ;	
Error:
	ssp_flag = 0;
	IB_CON1 = 0x00;
	IB_CON2 = 0x00;
	IB_CON3 = 0x00;
	IB_CON4 = 0x00;
	IB_CON5 = 0x00;
	// 切回Flash区
	FLASHCON = 0x00;
	// 操作结束，恢复现场
    _pop_(IEN0);
}
