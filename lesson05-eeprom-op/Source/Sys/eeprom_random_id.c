#include "eeprom_random_id.h"
#include "ABSACC.H"

/**
* @brief 读取随机识别码
*/
void readRandomID(unsigned char *r){
    // 访问类EEPROM
    FLASHCON = 0x01;
    r[0] = CBYTE[0x127b];
    r[1] = CBYTE[0x127c];
    r[2] = CBYTE[0x127d];
    r[3] = CBYTE[0x127e];
    r[4] = CBYTE[0x127f];
    // 访问 FLASH
    FLASHCON = 0x00;
}
