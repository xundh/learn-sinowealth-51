#include "irq_util.h"

/**
* @brief: 开启所有中断
*/
void enAllIrq(void)
{
    IEN0 |= 0x80;
}
/**
* @brief: 开启PCA0中断
*/
void enablePca0Irq(void)
{
    IEN2 |= 0x20;
}
