#include "SH79F9476.h"
#include "cpu.h"
#include "intrins.h"
#include "api_ext.h"
#include "clk_util.h"
#include "eeprom_op.h"

void main()
{
  UCHAR val;
	// 选择高速时钟
	highFrequenceClk();
	
	// 擦除扇区，0扇区 地址0-200H
	ssp_flag = 0xA5;
  EEPromSectorErase(0);
	
  // EEPROM 0x0100地址写值
  ssp_flag = 0x5A;
  EEPromByteProgram(0x01,0x00,0x67);
	
  //读取EEPROM 0x0100地址的值
  val=EEPromByteRead(0x01,0x00);

	while (1);
}
